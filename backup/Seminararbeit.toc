\contentsline {section}{\numberline {1}Notation}{iii}
\contentsline {section}{\numberline {2}Einleitung}{1}
\contentsline {subparagraph}{Versuch 2}{1}
\contentsline {section}{\numberline {3}Mathematische Grundlagen}{1}
\contentsline {subsection}{\numberline {3.1}Skalare, Vektoren, Matrizen und Tensoren}{3}
\contentsline {subsection}{\numberline {3.2}Rechenoperationen}{3}
\contentsline {subsection}{\numberline {3.3}Normen}{3}
\contentsline {subsection}{\numberline {3.4}Gradienten}{3}
\contentsline {subsection}{\numberline {3.5}Spezielle Matrizen}{3}
\contentsline {section}{\numberline {4}Machine Learning Grundlagen}{3}
\contentsline {subsection}{\numberline {4.1}\IeC {\"U}berwachtes Lernen}{3}
\contentsline {subsection}{\numberline {4.2}Un\IeC {\"u}berwachtes Lernen}{4}
\contentsline {subsection}{\numberline {4.3}Best\IeC {\"a}rkendes Lernen}{4}
\contentsline {subsection}{\numberline {4.4}Lineare Regression}{4}
\contentsline {subsection}{\numberline {4.5}Logistische Regression}{5}
\contentsline {subsection}{\numberline {4.6}Support Vector Machines}{5}
\contentsline {section}{\numberline {5}Neuronale Netze}{5}
\contentsline {subsection}{\numberline {5.1}Feedforward neuronale Netze}{6}
\contentsline {subsection}{\numberline {5.2}Perzeptron}{8}
\contentsline {subsection}{\numberline {5.3}Sigmoid Neuron}{8}
\contentsline {subsection}{\numberline {5.4}Gradientenabstiegsverfahren}{9}
\contentsline {subsubsection}{\numberline {5.4.1}Die Kostenfunktion}{9}
\contentsline {subsection}{\numberline {5.5}Backpropagation}{10}
\contentsline {section}{\numberline {6}Erkennung handschriftlicher Zahlen}{11}
\contentsline {subsection}{\numberline {6.1}Python Implementierung}{11}
\contentsline {section}{\numberline {7}Schluss}{12}
\contentsline {paragraph}{Tempor\IeC {\"a}re Fragmente}{12}
