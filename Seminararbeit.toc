\babel@toc {german}{}
\contentsline {section}{\numberline {1}Notation}{iii}
\contentsline {section}{\numberline {2}Einleitung}{1}
\contentsline {subparagraph}{Versuch 2}{1}
\contentsline {section}{\numberline {3}Mathematische Grundlagen}{3}
\contentsline {subsection}{\numberline {3.1}Skalare, Vektoren, Matrizen und Tensoren}{3}
\contentsline {paragraph}{Skalare}{3}
\contentsline {paragraph}{Vektoren}{3}
\contentsline {paragraph}{Matrizen}{3}
\contentsline {paragraph}{Tensoren}{4}
\contentsline {subsection}{\numberline {3.2}Matrix Algebra}{4}
\contentsline {paragraph}{Transponierung}{4}
\contentsline {paragraph}{Matrix Multiplikation}{4}
\contentsline {paragraph}{Hadamard Produkt}{5}
\contentsline {subsection}{\numberline {3.3}Normen}{5}
\contentsline {subsection}{\numberline {3.4}Partielle Ableitungen und Gradienten}{5}
\contentsline {subsection}{\numberline {3.5}Spezielle Matrizen}{5}
\contentsline {section}{\numberline {4}Machine Learning Grundlagen}{5}
\contentsline {subsection}{\numberline {4.1}Aufgaben}{6}
\contentsline {subsection}{\numberline {4.2}\IeC {\"U}berwachtes Lernen}{6}
\contentsline {subsection}{\numberline {4.3}Un\IeC {\"u}berwachtes Lernen}{7}
\contentsline {subsection}{\numberline {4.4}Best\IeC {\"a}rkendes Lernen}{7}
\contentsline {subsection}{\numberline {4.5}Lineare Regression}{7}
\contentsline {subsubsection}{\numberline {4.5.1}Regularisierung}{8}
\contentsline {subsubsection}{\numberline {4.5.2}Overfitting und underfitting}{8}
\contentsline {section}{\numberline {5}K\IeC {\"u}nstliche neuronale Netze}{10}
\contentsline {subsection}{\numberline {5.1}Feedforward neuronale Netze}{10}
\contentsline {subsection}{\numberline {5.2}Perzeptron}{12}
\contentsline {subsection}{\numberline {5.3}Sigmoid Neuron}{13}
\contentsline {subsection}{\numberline {5.4}Kostenfunktion}{13}
\contentsline {subsection}{\numberline {5.5}Gradientenverfahren}{14}
\contentsline {subsubsection}{\numberline {5.5.1}Ablauf}{15}
\contentsline {subsubsection}{\numberline {5.5.2}Mini-batch Stochastic Gradient Descent}{16}
\contentsline {subsection}{\numberline {5.6}Backpropagation}{17}
\contentsline {subsubsection}{\numberline {5.6.1}Ablauf der Fehlerr\IeC {\"u}ckf\IeC {\"u}hrung}{17}
\contentsline {subsubsection}{\numberline {5.6.2}Herleitung der Formeln}{18}
\contentsline {paragraph}{BP1}{18}
\contentsline {paragraph}{BP2}{19}
\contentsline {paragraph}{BP3}{20}
\contentsline {paragraph}{BP4}{20}
\contentsline {section}{\numberline {6}Erkennung handschriftlicher Zahlen}{20}
\contentsline {subsection}{\numberline {6.1}Python Implementierung}{20}
\contentsline {section}{\numberline {7}Schluss}{22}
