\babel@toc {nil}{}
\contentsline {section}{\numberline {1}Notation}{iii}
\contentsline {section}{\numberline {2}Einleitung}{1}
\contentsline {subparagraph}{Versuch 2}{1}
\contentsline {section}{\numberline {3}Mathematische Grundlagen}{3}
\contentsline {subsection}{\numberline {3.1}Skalare, Vektoren, Matrizen und Tensoren}{3}
\contentsline {paragraph}{Skalare}{3}
\contentsline {paragraph}{Vektoren}{3}
\contentsline {paragraph}{Matrizen}{3}
\contentsline {paragraph}{Tensoren}{4}
\contentsline {subsection}{\numberline {3.2}Matrix Algebra}{4}
\contentsline {paragraph}{Transponierung}{4}
\contentsline {paragraph}{Matrix Multiplikation}{4}
\contentsline {paragraph}{Hadamard Produkt}{4}
\contentsline {subsection}{\numberline {3.3}Normen}{5}
\contentsline {subsection}{\numberline {3.4}Partielle Ableitungen und Gradienten}{5}
\contentsline {subsection}{\numberline {3.5}Spezielle Matrizen}{5}
\contentsline {section}{\numberline {4}Machine Learning Grundlagen}{5}
\contentsline {subsection}{\numberline {4.1}Aufgaben}{5}
\contentsline {subsection}{\numberline {4.2}\IeC {\"U}berwachtes Lernen}{6}
\contentsline {subsection}{\numberline {4.3}Un\IeC {\"u}berwachtes Lernen}{6}
\contentsline {subsection}{\numberline {4.4}Best\IeC {\"a}rkendes Lernen}{7}
\contentsline {subsection}{\numberline {4.5}Lineare Regression}{7}
\contentsline {subsection}{\numberline {4.6}Logistische Regression}{8}
\contentsline {section}{\numberline {5}K\IeC {\"u}nstliche neuronale Netze}{8}
\contentsline {subsection}{\numberline {5.1}Feedforward neuronale Netze}{9}
\contentsline {subsection}{\numberline {5.2}Perzeptron}{9}
\contentsline {subsection}{\numberline {5.3}Sigmoid Neuron}{10}
\contentsline {subsection}{\numberline {5.4}Gradientenverfahren}{12}
\contentsline {subsubsection}{\numberline {5.4.1}Der Mittlere quadratische Fehler als Kostenfunktion}{12}
\contentsline {subsubsection}{\numberline {5.4.2}Ablauf}{13}
\contentsline {subsubsection}{\numberline {5.4.3}Stochastic Gradient Descent}{14}
\contentsline {subsection}{\numberline {5.5}Backpropagation}{14}
\contentsline {subsubsection}{\numberline {5.5.1}Formeln der Backpropagation}{15}
\contentsline {subsubsection}{\numberline {5.5.2}Ablauf der Fehlerr\IeC {\"u}ckf\IeC {\"u}hrung}{15}
\contentsline {subsubsection}{\numberline {5.5.3}Herleitung der Formeln}{16}
\contentsline {subsection}{\numberline {5.6}Regularisierung}{18}
\contentsline {subsubsection}{\numberline {5.6.1}Overfitting und underfitting}{18}
\contentsline {section}{\numberline {6}Erkennung handschriftlicher Zahlen}{18}
\contentsline {subsection}{\numberline {6.1}Python Implementierung}{19}
\contentsline {section}{\numberline {7}Schluss}{19}
