import numpy as np
from collections import deque


class ChessBoard(object):
    def __init__(self, m, n):
        self.m = m
        self.n = n


        self.matrix = np.zeros([m,n])
        self.checked = np.zeros([m, n])

    def update_knight_path_cost(self, pos_m, pos_n, maxpath):
        '''
        Berechnet die Pfade für alle Felder oder bis maxpath erreicht ist
        :param m: m Koordinate
        :param n: n Koordinate
        ;param maxpath; Gibt die maximale Pfadlänge an
        :return:
        '''

        cost = 2

        initial_moves = self.get_possible_knight_moves(pos_m, pos_n)
        path_queue = deque([[initial_moves, cost, pos_m, pos_n]])

        self.matrix[pos_m][pos_n] = 1
        self.checked[pos_m][pos_n] = 0

        while path_queue:
            moves, cost, current_m, current_n = path_queue.popleft()

            if self.checked[current_m][current_n] == 1:
                # print("already checked, skipping", current_m, current_n)
                continue

            for new_m, new_n in moves:
                value = self.matrix[new_m][new_n]

                if value == 0 or cost < value:
                    self.matrix[new_m][new_n] = cost

                if self.checked[new_m][new_n] == 0:
                    new_moves = self.get_possible_knight_moves(new_m, new_n)

                    if new_moves:
                        # print("Let me enqueue", new_moves, "this is for", new_m, "and", new_n)
                        path_queue.append([new_moves, cost + 1, new_m, new_n])

            self.checked[current_m][current_n] = 1


    def get_possible_knight_moves(self, pos_m, pos_n):
        moves = []

        possible_moves = (2,1), (-2,1), (2,-1), (-2,-1), \
                         (1,2), (-1,2), (1,-2), (-1,-2)

        for move in possible_moves:
            new_m, new_n = pos_m + move[0], pos_n + move[1]
            if 0 <= new_m < self.m and 0 <= new_n < self.n:
                moves.append((new_m, new_n))

        return moves


for i in range(3,300):
    size = i
    board = ChessBoard(size, size)
    board.update_knight_path_cost(size - 1, 0, 0)
    # print(board.matrix)

    board2 = ChessBoard(size, size)
    board2.update_knight_path_cost(size - 1, 0, 0)
    # print(board2.matrix)

    if board.matrix[size-1][size-1] == board2.matrix[0][size - 1]:
        print("Size: ", size, "| moves for a:",  board.matrix[size-1][size-1], "| moves for b: ", board2.matrix[0][size - 1], "match!")
    else:
        print("Size: ", size, "| moves for a:", board.matrix[size - 1][size - 1], "| moves for b: ",
              board2.matrix[0][size - 1])


print(7)

size = 7
board = ChessBoard(size, size)
board.update_knight_path_cost(size - 1, 0, 0)
print(board.matrix)

board2 = ChessBoard(size, size)
board2.update_knight_path_cost(size - 1, size - 1, 0)
print(board2.matrix)
