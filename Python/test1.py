from manimlib.imports import *
import numpy as np


class SquareToCircle(Scene):
    def construct(self):
        circle = Circle()
        square = Square()
        square.flip(RIGHT)
        square.rotate(-3 * TAU / 8)
        circle.set_fill(PINK, opacity=0.5)

        self.play(ShowCreation(square))
        self.play(Transform(square, circle))
        self.play(FadeOut(square))


class NeuralNetworkConstruction(Scene):
    def construct(self):
        circle = Circle()
        line1 = Line(np.array([-5,2,0]),np.array([0,0,0]))
        line2 = Line(np.array([-5, 0, 0]), np.array([0, 0, 0]))
        line3 = Line(np.array([-5, -2, 0]), np.array([0, 0, 0]))

        self.play(ShowCreation(circle))
        self.add(line1)
        self.wait(0.5)
        self.add(line2)
        self.play(ShowCreation(line3))


class NeuralNetwork(Scene):
    def construct(self):
        size = [6, 8, 8, 4]
        startpoint = np.array([-3, -3, 0])
        vspace = 0.8
        hspace = 2

        neurons = []

        # Platziere Neuronen auf dem Raster
        depth = len(size)
        maxsize = max(size)

        for l in range(depth):
            neurons.append([])
            for j in range(size[l]):
                vspacemargin = (maxsize - size[l]) / 2

                point = np.array([startpoint[0] + l*hspace, startpoint[1] + (vspacemargin + j)*vspace, startpoint[2]])
                neuron = Neuron().move_to(point) # wäre sinnvoll wenn neuronenobjekte einen layer hätten
                neurons[l].append(neuron)
                print("circle", l, j)
                #self.add(neuron)
                self.add_foreground_mobject(neuron)

        placeholder = Circle().move_to( np.array([1000,1000,0]))

        self.play(ShowCreation(placeholder))


        # Verbinde Neuronen
        for l in range(len(neurons)):
            for neuron in neurons[l]:
                if (l+1) < depth:
                    x = neuron.get_x()
                    y = neuron.get_y()
                    z = neuron.get_z()

                    for neuron_target in neurons[l+1]:
                        x_target = neuron_target.get_x()
                        y_target = neuron_target.get_y()
                        z_target = neuron_target.get_z()
                        line = Line(np.array([x, y, z - 1]), np.array([x_target, y_target, z_target - 1]))
                        # print(x, y, z, 'to', x_target, y_target, z_target)
                        self.wait(0.06 - l * 0.01)

                        self.add(line)

        self.play(ShowCreation(placeholder))



class Neuron(Circle):
    CONFIG = {
    "radius" : 0.2,
    "stroke_width" : 3,
    "color" : WHITE,
    "fill_color" : BLACK,
    "fill_opacity" : 1,
    }
    def __init__(self, **kwargs):
        Circle.__init__(self, **kwargs)
        plus = TexMobject("+")
        plus.scale(0.7)
        plus.move_to(self)
        # self.add(plus)



class MovingCharges(Scene):
    CONFIG = {
    "plane_kwargs" : {
        "color" : RED_B
        },
    "point_charge_loc" : 0.5*RIGHT-1.5*UP,
    }
    def construct(self):
        plane = NumberPlane(**self.plane_kwargs)
        plane.add(plane.get_axis_labels())
        self.add(plane)

        field = VGroup(*[self.calc_field(x*RIGHT+y*UP)
            for x in np.arange(-9,9,1)
            for y in np.arange(-5,5,1)
            ])
        self.field=field
        source_charge = self.Positron().move_to(self.point_charge_loc)
        self.play(ShowCreation(source_charge))
        self.play(ShowCreation(field))
        self.moving_charge()

    def calc_field(self,point):
        x,y = point[:2]
        Rx,Ry = self.point_charge_loc[:2]
        r = math.sqrt((x-Rx)**2 + (y-Ry)**2)
        efield = (point - self.point_charge_loc)/r**3
        return Vector(efield).shift(point)

    def moving_charge(self):
        numb_charges=4
        possible_points = [v.get_start() for v in self.field]
        points = random.sample(possible_points, numb_charges)
        particles = VGroup(*[
            self.Positron().move_to(point)
            for point in points
        ])
        for particle in particles:
            particle.velocity = np.array((0,0,0))

        self.play(FadeIn(particles))
        self.moving_particles = particles
        self.add_foreground_mobjects(self.moving_particles )
        self.always_continually_update = True
        self.always_update_mobjects = True
        self.wait(10)

    def field_at_point(self,point):
        x,y = point[:2]
        Rx,Ry = self.point_charge_loc[:2]
        r = math.sqrt((x-Rx)**2 + (y-Ry)**2)
        efield = (point - self.point_charge_loc)/r**3
        return efield

    def update_mobjects(self, *args, **kwargs):
        if hasattr(self, "moving_particles"):
            #dt = self.frame_duration
            dt = 1 / 15 # Framerate bei niedriger Qualität

            for p in self.moving_particles:
                accel = self.field_at_point(p.get_center())
                p.velocity = p.velocity + accel*dt
                p.shift(p.velocity*dt)


    class Positron(Circle):
        CONFIG = {
        "radius" : 0.2,
        "stroke_width" : 3,
        "color" : RED,
        "fill_color" : RED,
        "fill_opacity" : 0.5,
        }
        def __init__(self, **kwargs):
            Circle.__init__(self, **kwargs)
            plus = TexMobject("+")
            plus.scale(0.7)
            plus.move_to(self)
            self.add(plus)
