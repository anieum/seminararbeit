import os
from itertools import permutations

n = 5  # total runs per config
epoch = 30  # number of epochs per run

# Configs to test
hidden_layers = 0, 1, 2
neurons_in_hidden_layers = 10, 20, 30, 60
batch_sizes = 10, 50, 100
learning_rates = 0.5, 1.0, 3.0, 5.0

# Possible dimensions
dimensions = []

for number_of_hidden_layers in hidden_layers:
    for permutation in permutations(neurons_in_hidden_layers, number_of_hidden_layers):
        dimension = [784]

        for el in permutation:
            dimension.append(el)

        dimension.append(10)

        dimensions.append(dimension)


# Total runs required
t = n * len(dimensions) * len(batch_sizes) * len(learning_rates)

print("Number of runs required:", t)

configs = []
for d in dimensions:
    for b in batch_sizes:
        for l in learning_rates:
            configs.append([d, b, l])

print(len(configs))

print(configs)

# subprocess.call(['./neuralnetwork_test.py', dimension, batch_size, learning_rate, number_of_epochs])
z = 0
for i in range(n):
    for config in configs:
        z += 1

        outfile = "log\\" + str(z) + "c.txt"
        path = "neuralnetwork_test.py "

        for e in config:
            path += str(e).replace(" ", "") + " "

        path += str(epoch)
        path += " > " + outfile

        print(path)
        os.system(path)