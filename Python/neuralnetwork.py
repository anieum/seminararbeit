import random
import numpy as np

from lib import mnist_loader

import sys

print(sys.version, "\n")

'''
Implementierung eines neuronalen Feedforward Netzes
'''


# Mathematische Funktionen:
def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def derivative_sigmoid(z):
    return sigmoid(z) * (1 - sigmoid(z))


# Sonstige Funktionen:
def cost(activation_value, target_value):
    a, y = activation_value, target_value

    return 0.5 * np.linalg.norm(y - a) ** 2


def split(list, size):
    return [list[i * size:(i + 1) * size] for i in range((len(list) + size - 1) // size)]


def log(text):
    log = False

    if log:
        print(text)


def visualize_number(vector):
    output = ""

    for i in range(784):
        if i % 28 == 0:
            output += "\n"

        if vector[i] > 0.5:
            output += "@"
        else:
            output += " "

    print(output)


class NeuralNetwork(object):
    def __init__(self, dimensions):
        # Initialisiere neuronales Netz mit zufälligen Bias und Gewichten
        self.dimensions = {i: dimensions[i] for i in range(len(dimensions))}
        self.layers = list(range(len(dimensions)))
        self.epoch_counter = 0

        self.biases = {0: None}  # Erste Schicht hat keine Bias
        self.weights = {0: None}  # Erste Schicht hat keine Gewichte

        for l in range(1, len(dimensions)):
            self.biases[l] = np.random.randn(self.dimensions[l], 1)  # Biasvektor für Schicht l
            self.weights[l] = np.random.randn(self.dimensions[l],
                                              self.dimensions[l - 1])  # Weightmatrix zwischen Schicht l und l-1

    def train_using_sgd(self, input_data, batch_size, learning_rate):
        random.shuffle(input_data)
        mini_batches = split(input_data, batch_size)

        for mini_batch in mini_batches:
            gradient_b, gradient_w = self.process_batch(mini_batch)

            self.update_parameters(gradient_b, gradient_w, learning_rate)

        self.epoch_counter += 1

    def process_batch(self, input_data):
        results = []

        # Testeinheiten Vorwärtsdurchgang
        for x, y in input_data:
            result = self.feedforward(x, y)
            results.append(result)

        # Rückführung des Fehlers
        individual_gradients_b = {l: [] for l in self.layers[1::]}
        individual_gradients_w = {l: [] for l in self.layers[1::]}
        total_gradient_b, total_gradient_w = {}, {}

        for x, y, all_zs, all_as in results:
            # Berechne den Gradienten je Sample
            grad_b, grad_w = self.backpropagate_error(x, y, all_zs, all_as)

            # Fasse die einzelnen Gradienten je Layer zusammen
            for l in self.layers[1::]:
                individual_gradients_b[l].append(grad_b[l])
                individual_gradients_w[l].append(grad_w[l])

        # Berechne die durchschnittlichen Gradienten
        for l in self.layers[1::]:
            total_gradient_b[l] = np.mean(individual_gradients_b[l], axis=0)
            total_gradient_w[l] = np.mean(individual_gradients_w[l], axis=0)

        return total_gradient_b, total_gradient_w

    def feedforward(self, input_data, target):
        x, y = input_data, target

        weighted_inputs, activation_values = {}, {}
        activation_values[0] = x

        # Je Schicht
        for l in self.layers[1::]:
            b = self.biases[l]
            w = self.weights[l]

            # Gewichtete Eingabe und Aktivierung berechnen
            z = np.dot(w, x) + b
            x = sigmoid(z)

            # Werte zwischenspeichern
            weighted_inputs[l] = z
            activation_values[l] = x

        return x, y, weighted_inputs, activation_values

    def backpropagate_error(self, output, target, weighted_inputs, activation_values):
        gradient_b, gradient_w, error = {}, {}, {}

        L = self.layers[-1]

        # Berechne Fehler in der letzten Schicht L
        a, y, z = output, target, weighted_inputs[L]

        error[L] = np.multiply((a - y), derivative_sigmoid(z))  # BP1

        # Führe den Fehler schichtweise rückwärts zurück.
        # Beginne bei L - 1, dann L - 2, usw.
        for l in list(reversed(self.layers))[1:-1]:
            w, d, z = self.weights, error, weighted_inputs[l]

            error[l] = np.multiply(np.dot(w[l + 1].transpose(), d[l + 1]),
                                   derivative_sigmoid(z))  #BP2

        for l in self.layers[1::]:
            gradient_b[l] = error[l]  # BP3
            # gradient_w[l] = np.dot(error[l],
            #                       activation_values[l - 1].transpose())  # BP4
            gradient_w[l] = np.dot(error[l], activation_values[l - 1].transpose())  # BP4

        return gradient_b, gradient_w

    def test_accuracy(self, input_data):
        identified = 0

        for x, y in input_data:
            result = self.feedforward(x, y)

            predicted_number = np.argmax(result[0])

            if predicted_number == y:
                identified += 1

        print("Epoch: {0}; Identified: {1} / {2}".format(
            self.epoch_counter, identified, len(input_data)
        ))

    def update_parameters(self, gradient_b, gradient_w, learning_rate):
        for l in self.layers[1::]:
            self.biases[l] = self.biases[l] - learning_rate * gradient_b[l]
            self.weights[l] = self.weights[l] - learning_rate * gradient_w[l]


# Erstelle, trainiere und teste Netz
print("Loading MNIST database")
training_data, validation_data, test_data = \
    [list(data) for data in mnist_loader.load_data_wrapper()]


# visualize_number(test_data[10][0])

print("Trainingsamples:", len(training_data), "Testsamples:", len(test_data))

network = NeuralNetwork([784, 60, 10])

print("Initialized network with", len(network.layers), "layers")
print("Dimensions: ", network.dimensions, "\n")

batch_size = 10
learning_rate = 5.0

print("Starting training with batchsize", batch_size, "and learningrate", learning_rate)
network.test_accuracy(test_data)

while True:
    network.train_using_sgd(training_data, batch_size, learning_rate)
    network.test_accuracy(test_data)
