3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 30, 3: 10} 

Starting training with batchsize 100 and learningrate 1.0
Epoch: 0; Identified: 962 / 10000
Epoch: 1; Identified: 5323 / 10000
Epoch: 2; Identified: 6926 / 10000
Epoch: 3; Identified: 7717 / 10000
Epoch: 4; Identified: 8087 / 10000
Epoch: 5; Identified: 8331 / 10000
Epoch: 6; Identified: 8432 / 10000
Epoch: 7; Identified: 8538 / 10000
Epoch: 8; Identified: 8625 / 10000
Epoch: 9; Identified: 8682 / 10000
Epoch: 10; Identified: 8724 / 10000
Epoch: 11; Identified: 8766 / 10000
Epoch: 12; Identified: 8788 / 10000
Epoch: 13; Identified: 8835 / 10000
Epoch: 14; Identified: 8849 / 10000
Epoch: 15; Identified: 8870 / 10000
Epoch: 16; Identified: 8908 / 10000
Epoch: 17; Identified: 8924 / 10000
Epoch: 18; Identified: 8940 / 10000
Epoch: 19; Identified: 8955 / 10000
Epoch: 20; Identified: 8962 / 10000
Epoch: 21; Identified: 8966 / 10000
Epoch: 22; Identified: 8986 / 10000
Epoch: 23; Identified: 8986 / 10000
Epoch: 24; Identified: 8994 / 10000
Epoch: 25; Identified: 9022 / 10000
Epoch: 26; Identified: 9035 / 10000
Epoch: 27; Identified: 9021 / 10000
Epoch: 28; Identified: 9052 / 10000
Epoch: 29; Identified: 9054 / 10000
Epoch: 30; Identified: 9051 / 10000
