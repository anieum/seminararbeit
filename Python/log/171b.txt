3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 10, 3: 10} 

Starting training with batchsize 10 and learningrate 3.0
Epoch: 0; Identified: 949 / 10000
Epoch: 1; Identified: 8885 / 10000
Epoch: 2; Identified: 9207 / 10000
Epoch: 3; Identified: 9299 / 10000
Epoch: 4; Identified: 9347 / 10000
Epoch: 5; Identified: 9322 / 10000
Epoch: 6; Identified: 9405 / 10000
Epoch: 7; Identified: 9420 / 10000
Epoch: 8; Identified: 9431 / 10000
Epoch: 9; Identified: 9327 / 10000
Epoch: 10; Identified: 9436 / 10000
Epoch: 11; Identified: 9459 / 10000
Epoch: 12; Identified: 9440 / 10000
Epoch: 13; Identified: 9468 / 10000
Epoch: 14; Identified: 9450 / 10000
Epoch: 15; Identified: 9468 / 10000
Epoch: 16; Identified: 9462 / 10000
Epoch: 17; Identified: 9446 / 10000
Epoch: 18; Identified: 9517 / 10000
Epoch: 19; Identified: 9459 / 10000
Epoch: 20; Identified: 9489 / 10000
Epoch: 21; Identified: 9504 / 10000
Epoch: 22; Identified: 9546 / 10000
Epoch: 23; Identified: 9445 / 10000
Epoch: 24; Identified: 9555 / 10000
Epoch: 25; Identified: 9551 / 10000
Epoch: 26; Identified: 9520 / 10000
Epoch: 27; Identified: 9545 / 10000
Epoch: 28; Identified: 9499 / 10000
Epoch: 29; Identified: 9521 / 10000
Epoch: 30; Identified: 9523 / 10000
