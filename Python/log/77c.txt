3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 30, 3: 10} 

Starting training with batchsize 50 and learningrate 0.5
Epoch: 0; Identified: 1292 / 10000
Epoch: 1; Identified: 4668 / 10000
Epoch: 2; Identified: 6041 / 10000
Epoch: 3; Identified: 6592 / 10000
Epoch: 4; Identified: 6917 / 10000
Epoch: 5; Identified: 7134 / 10000
Epoch: 6; Identified: 7377 / 10000
Epoch: 7; Identified: 7530 / 10000
Epoch: 8; Identified: 7645 / 10000
Epoch: 9; Identified: 7731 / 10000
Epoch: 10; Identified: 7800 / 10000
Epoch: 11; Identified: 8391 / 10000
Epoch: 12; Identified: 8509 / 10000
Epoch: 13; Identified: 8561 / 10000
Epoch: 14; Identified: 8610 / 10000
Epoch: 15; Identified: 8676 / 10000
Epoch: 16; Identified: 8701 / 10000
Epoch: 17; Identified: 8729 / 10000
Epoch: 18; Identified: 8750 / 10000
Epoch: 19; Identified: 8773 / 10000
Epoch: 20; Identified: 8796 / 10000
Epoch: 21; Identified: 8828 / 10000
Epoch: 22; Identified: 8830 / 10000
Epoch: 23; Identified: 8823 / 10000
Epoch: 24; Identified: 8857 / 10000
Epoch: 25; Identified: 8873 / 10000
Epoch: 26; Identified: 8890 / 10000
Epoch: 27; Identified: 8885 / 10000
Epoch: 28; Identified: 8892 / 10000
Epoch: 29; Identified: 8914 / 10000
Epoch: 30; Identified: 8918 / 10000
