3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 10, 3: 10} 

Starting training with batchsize 100 and learningrate 5.0
Epoch: 0; Identified: 1106 / 10000
Epoch: 1; Identified: 8017 / 10000
Epoch: 2; Identified: 8732 / 10000
Epoch: 3; Identified: 8919 / 10000
Epoch: 4; Identified: 9081 / 10000
Epoch: 5; Identified: 9167 / 10000
Epoch: 6; Identified: 9199 / 10000
Epoch: 7; Identified: 9242 / 10000
Epoch: 8; Identified: 9277 / 10000
Epoch: 9; Identified: 9307 / 10000
Epoch: 10; Identified: 9313 / 10000
Epoch: 11; Identified: 9364 / 10000
Epoch: 12; Identified: 9343 / 10000
Epoch: 13; Identified: 9366 / 10000
Epoch: 14; Identified: 9367 / 10000
Epoch: 15; Identified: 9403 / 10000
Epoch: 16; Identified: 9365 / 10000
Epoch: 17; Identified: 9406 / 10000
Epoch: 18; Identified: 9410 / 10000
Epoch: 19; Identified: 9398 / 10000
Epoch: 20; Identified: 9388 / 10000
Epoch: 21; Identified: 9415 / 10000
Epoch: 22; Identified: 9402 / 10000
Epoch: 23; Identified: 9417 / 10000
Epoch: 24; Identified: 9418 / 10000
Epoch: 25; Identified: 9422 / 10000
Epoch: 26; Identified: 9414 / 10000
Epoch: 27; Identified: 9430 / 10000
Epoch: 28; Identified: 9421 / 10000
Epoch: 29; Identified: 9422 / 10000
Epoch: 30; Identified: 9434 / 10000
