3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 60, 3: 10} 

Starting training with batchsize 100 and learningrate 3.0
Epoch: 0; Identified: 1230 / 10000
Epoch: 1; Identified: 6424 / 10000
Epoch: 2; Identified: 6923 / 10000
Epoch: 3; Identified: 7180 / 10000
Epoch: 4; Identified: 7255 / 10000
Epoch: 5; Identified: 7339 / 10000
Epoch: 6; Identified: 7406 / 10000
Epoch: 7; Identified: 8215 / 10000
Epoch: 8; Identified: 8279 / 10000
Epoch: 9; Identified: 8327 / 10000
Epoch: 10; Identified: 8359 / 10000
Epoch: 11; Identified: 8384 / 10000
Epoch: 12; Identified: 8398 / 10000
Epoch: 13; Identified: 8429 / 10000
Epoch: 14; Identified: 8432 / 10000
Epoch: 15; Identified: 8451 / 10000
Epoch: 16; Identified: 8454 / 10000
Epoch: 17; Identified: 8466 / 10000
Epoch: 18; Identified: 8470 / 10000
Epoch: 19; Identified: 8473 / 10000
Epoch: 20; Identified: 8469 / 10000
Epoch: 21; Identified: 8476 / 10000
Epoch: 22; Identified: 8492 / 10000
Epoch: 23; Identified: 8472 / 10000
Epoch: 24; Identified: 8491 / 10000
Epoch: 25; Identified: 8503 / 10000
Epoch: 26; Identified: 8522 / 10000
Epoch: 27; Identified: 8497 / 10000
Epoch: 28; Identified: 8511 / 10000
Epoch: 29; Identified: 8525 / 10000
Epoch: 30; Identified: 8532 / 10000
