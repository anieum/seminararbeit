3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 20, 3: 10} 

Starting training with batchsize 10 and learningrate 0.5
Epoch: 0; Identified: 489 / 10000
Epoch: 1; Identified: 8045 / 10000
Epoch: 2; Identified: 8656 / 10000
Epoch: 3; Identified: 8909 / 10000
Epoch: 4; Identified: 9015 / 10000
Epoch: 5; Identified: 9106 / 10000
Epoch: 6; Identified: 9176 / 10000
Epoch: 7; Identified: 9188 / 10000
Epoch: 8; Identified: 9193 / 10000
Epoch: 9; Identified: 9221 / 10000
Epoch: 10; Identified: 9248 / 10000
Epoch: 11; Identified: 9261 / 10000
Epoch: 12; Identified: 9289 / 10000
Epoch: 13; Identified: 9289 / 10000
Epoch: 14; Identified: 9291 / 10000
Epoch: 15; Identified: 9300 / 10000
Epoch: 16; Identified: 9320 / 10000
Epoch: 17; Identified: 9336 / 10000
Epoch: 18; Identified: 9338 / 10000
Epoch: 19; Identified: 9342 / 10000
Epoch: 20; Identified: 9359 / 10000
Epoch: 21; Identified: 9374 / 10000
Epoch: 22; Identified: 9351 / 10000
Epoch: 23; Identified: 9353 / 10000
Epoch: 24; Identified: 9385 / 10000
Epoch: 25; Identified: 9377 / 10000
Epoch: 26; Identified: 9367 / 10000
Epoch: 27; Identified: 9379 / 10000
Epoch: 28; Identified: 9382 / 10000
Epoch: 29; Identified: 9373 / 10000
Epoch: 30; Identified: 9386 / 10000
