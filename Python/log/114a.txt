3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 20, 2: 30, 3: 10} 

Starting training with batchsize 50 and learningrate 1.0
Epoch: 0; Identified: 1041 / 10000
Epoch: 1; Identified: 7097 / 10000
Epoch: 2; Identified: 8133 / 10000
Epoch: 3; Identified: 8522 / 10000
Epoch: 4; Identified: 8696 / 10000
Epoch: 5; Identified: 8805 / 10000
Epoch: 6; Identified: 8870 / 10000
Epoch: 7; Identified: 8931 / 10000
Epoch: 8; Identified: 8966 / 10000
Epoch: 9; Identified: 9000 / 10000
Epoch: 10; Identified: 9034 / 10000
Epoch: 11; Identified: 9042 / 10000
Epoch: 12; Identified: 9072 / 10000
Epoch: 13; Identified: 9100 / 10000
Epoch: 14; Identified: 9100 / 10000
Epoch: 15; Identified: 9113 / 10000
Epoch: 16; Identified: 9144 / 10000
Epoch: 17; Identified: 9148 / 10000
Epoch: 18; Identified: 9149 / 10000
Epoch: 19; Identified: 9150 / 10000
Epoch: 20; Identified: 9184 / 10000
Epoch: 21; Identified: 9180 / 10000
Epoch: 22; Identified: 9192 / 10000
Epoch: 23; Identified: 9194 / 10000
Epoch: 24; Identified: 9210 / 10000
Epoch: 25; Identified: 9226 / 10000
Epoch: 26; Identified: 9229 / 10000
Epoch: 27; Identified: 9226 / 10000
Epoch: 28; Identified: 9226 / 10000
Epoch: 29; Identified: 9258 / 10000
Epoch: 30; Identified: 9241 / 10000
