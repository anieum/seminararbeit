3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 30, 2: 10} 

Starting training with batchsize 10 and learningrate 0.5
Epoch: 0; Identified: 953 / 10000
Epoch: 1; Identified: 7672 / 10000
Epoch: 2; Identified: 8056 / 10000
Epoch: 3; Identified: 8193 / 10000
Epoch: 4; Identified: 8257 / 10000
Epoch: 5; Identified: 8286 / 10000
Epoch: 6; Identified: 8353 / 10000
Epoch: 7; Identified: 8361 / 10000
Epoch: 8; Identified: 8400 / 10000
Epoch: 9; Identified: 8435 / 10000
Epoch: 10; Identified: 8430 / 10000
Epoch: 11; Identified: 8422 / 10000
Epoch: 12; Identified: 8469 / 10000
Epoch: 13; Identified: 8465 / 10000
Epoch: 14; Identified: 8481 / 10000
Epoch: 15; Identified: 8477 / 10000
Epoch: 16; Identified: 8492 / 10000
Epoch: 17; Identified: 8478 / 10000
Epoch: 18; Identified: 8488 / 10000
Epoch: 19; Identified: 8489 / 10000
Epoch: 20; Identified: 8501 / 10000
Epoch: 21; Identified: 8507 / 10000
Epoch: 22; Identified: 8502 / 10000
Epoch: 23; Identified: 8505 / 10000
Epoch: 24; Identified: 8500 / 10000
Epoch: 25; Identified: 8513 / 10000
Epoch: 26; Identified: 8518 / 10000
Epoch: 27; Identified: 8509 / 10000
Epoch: 28; Identified: 8510 / 10000
Epoch: 29; Identified: 8518 / 10000
Epoch: 30; Identified: 8517 / 10000
