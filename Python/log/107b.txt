3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 20, 2: 10, 3: 10} 

Starting training with batchsize 100 and learningrate 3.0
Epoch: 0; Identified: 972 / 10000
Epoch: 1; Identified: 5663 / 10000
Epoch: 2; Identified: 7637 / 10000
Epoch: 3; Identified: 8282 / 10000
Epoch: 4; Identified: 8527 / 10000
Epoch: 5; Identified: 8712 / 10000
Epoch: 6; Identified: 8828 / 10000
Epoch: 7; Identified: 8874 / 10000
Epoch: 8; Identified: 8969 / 10000
Epoch: 9; Identified: 9028 / 10000
Epoch: 10; Identified: 9034 / 10000
Epoch: 11; Identified: 9094 / 10000
Epoch: 12; Identified: 9108 / 10000
Epoch: 13; Identified: 9144 / 10000
Epoch: 14; Identified: 9154 / 10000
Epoch: 15; Identified: 9174 / 10000
Epoch: 16; Identified: 9177 / 10000
Epoch: 17; Identified: 9203 / 10000
Epoch: 18; Identified: 9205 / 10000
Epoch: 19; Identified: 9213 / 10000
Epoch: 20; Identified: 9208 / 10000
Epoch: 21; Identified: 9220 / 10000
Epoch: 22; Identified: 9229 / 10000
Epoch: 23; Identified: 9245 / 10000
Epoch: 24; Identified: 9228 / 10000
Epoch: 25; Identified: 9245 / 10000
Epoch: 26; Identified: 9220 / 10000
Epoch: 27; Identified: 9235 / 10000
Epoch: 28; Identified: 9249 / 10000
Epoch: 29; Identified: 9258 / 10000
Epoch: 30; Identified: 9261 / 10000
