3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 30, 2: 10} 

Starting training with batchsize 100 and learningrate 0.5
Epoch: 0; Identified: 669 / 10000
Epoch: 1; Identified: 3020 / 10000
Epoch: 2; Identified: 4358 / 10000
Epoch: 3; Identified: 4868 / 10000
Epoch: 4; Identified: 5364 / 10000
Epoch: 5; Identified: 5912 / 10000
Epoch: 6; Identified: 6290 / 10000
Epoch: 7; Identified: 6596 / 10000
Epoch: 8; Identified: 7121 / 10000
Epoch: 9; Identified: 7409 / 10000
Epoch: 10; Identified: 7805 / 10000
Epoch: 11; Identified: 8272 / 10000
Epoch: 12; Identified: 8467 / 10000
Epoch: 13; Identified: 8577 / 10000
Epoch: 14; Identified: 8643 / 10000
Epoch: 15; Identified: 8699 / 10000
Epoch: 16; Identified: 8746 / 10000
Epoch: 17; Identified: 8772 / 10000
Epoch: 18; Identified: 8793 / 10000
Epoch: 19; Identified: 8821 / 10000
Epoch: 20; Identified: 8848 / 10000
Epoch: 21; Identified: 8861 / 10000
Epoch: 22; Identified: 8892 / 10000
Epoch: 23; Identified: 8899 / 10000
Epoch: 24; Identified: 8933 / 10000
Epoch: 25; Identified: 8941 / 10000
Epoch: 26; Identified: 8951 / 10000
Epoch: 27; Identified: 8966 / 10000
Epoch: 28; Identified: 8975 / 10000
Epoch: 29; Identified: 8979 / 10000
Epoch: 30; Identified: 8988 / 10000
