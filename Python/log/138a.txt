3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 10, 3: 10} 

Starting training with batchsize 50 and learningrate 1.0
Epoch: 0; Identified: 936 / 10000
Epoch: 1; Identified: 3969 / 10000
Epoch: 2; Identified: 6202 / 10000
Epoch: 3; Identified: 6773 / 10000
Epoch: 4; Identified: 7435 / 10000
Epoch: 5; Identified: 7788 / 10000
Epoch: 6; Identified: 8024 / 10000
Epoch: 7; Identified: 8452 / 10000
Epoch: 8; Identified: 8633 / 10000
Epoch: 9; Identified: 8782 / 10000
Epoch: 10; Identified: 8874 / 10000
Epoch: 11; Identified: 8947 / 10000
Epoch: 12; Identified: 8995 / 10000
Epoch: 13; Identified: 9013 / 10000
Epoch: 14; Identified: 9052 / 10000
Epoch: 15; Identified: 9086 / 10000
Epoch: 16; Identified: 9110 / 10000
Epoch: 17; Identified: 9137 / 10000
Epoch: 18; Identified: 9156 / 10000
Epoch: 19; Identified: 9155 / 10000
Epoch: 20; Identified: 9163 / 10000
Epoch: 21; Identified: 9179 / 10000
Epoch: 22; Identified: 9178 / 10000
Epoch: 23; Identified: 9185 / 10000
Epoch: 24; Identified: 9201 / 10000
Epoch: 25; Identified: 9229 / 10000
Epoch: 26; Identified: 9232 / 10000
Epoch: 27; Identified: 9225 / 10000
Epoch: 28; Identified: 9221 / 10000
Epoch: 29; Identified: 9239 / 10000
Epoch: 30; Identified: 9257 / 10000
