3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 60, 2: 10} 

Starting training with batchsize 100 and learningrate 5.0
Epoch: 0; Identified: 971 / 10000
Epoch: 1; Identified: 6132 / 10000
Epoch: 2; Identified: 7068 / 10000
Epoch: 3; Identified: 8208 / 10000
Epoch: 4; Identified: 8307 / 10000
Epoch: 5; Identified: 8369 / 10000
Epoch: 6; Identified: 8436 / 10000
Epoch: 7; Identified: 8443 / 10000
Epoch: 8; Identified: 8484 / 10000
Epoch: 9; Identified: 8501 / 10000
Epoch: 10; Identified: 8507 / 10000
Epoch: 11; Identified: 8518 / 10000
Epoch: 12; Identified: 8547 / 10000
Epoch: 13; Identified: 8549 / 10000
Epoch: 14; Identified: 8556 / 10000
Epoch: 15; Identified: 8567 / 10000
Epoch: 16; Identified: 8582 / 10000
Epoch: 17; Identified: 8581 / 10000
Epoch: 18; Identified: 8603 / 10000
Epoch: 19; Identified: 8601 / 10000
Epoch: 20; Identified: 8617 / 10000
Epoch: 21; Identified: 8611 / 10000
Epoch: 22; Identified: 8616 / 10000
Epoch: 23; Identified: 8616 / 10000
Epoch: 24; Identified: 8630 / 10000
Epoch: 25; Identified: 8617 / 10000
Epoch: 26; Identified: 8636 / 10000
Epoch: 27; Identified: 8630 / 10000
Epoch: 28; Identified: 8623 / 10000
Epoch: 29; Identified: 8644 / 10000
Epoch: 30; Identified: 8712 / 10000
