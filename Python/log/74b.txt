3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 30, 3: 10} 

Starting training with batchsize 10 and learningrate 1.0
Epoch: 0; Identified: 1036 / 10000
Epoch: 1; Identified: 8450 / 10000
Epoch: 2; Identified: 8868 / 10000
Epoch: 3; Identified: 8990 / 10000
Epoch: 4; Identified: 8959 / 10000
Epoch: 5; Identified: 9076 / 10000
Epoch: 6; Identified: 9125 / 10000
Epoch: 7; Identified: 9108 / 10000
Epoch: 8; Identified: 9141 / 10000
Epoch: 9; Identified: 9147 / 10000
Epoch: 10; Identified: 9171 / 10000
Epoch: 11; Identified: 9150 / 10000
Epoch: 12; Identified: 9182 / 10000
Epoch: 13; Identified: 9193 / 10000
Epoch: 14; Identified: 9175 / 10000
Epoch: 15; Identified: 9190 / 10000
Epoch: 16; Identified: 9174 / 10000
Epoch: 17; Identified: 9184 / 10000
Epoch: 18; Identified: 9207 / 10000
Epoch: 19; Identified: 9211 / 10000
Epoch: 20; Identified: 9227 / 10000
Epoch: 21; Identified: 9196 / 10000
Epoch: 22; Identified: 9227 / 10000
Epoch: 23; Identified: 9197 / 10000
Epoch: 24; Identified: 9227 / 10000
Epoch: 25; Identified: 9217 / 10000
Epoch: 26; Identified: 9210 / 10000
Epoch: 27; Identified: 9207 / 10000
Epoch: 28; Identified: 9207 / 10000
Epoch: 29; Identified: 9242 / 10000
Epoch: 30; Identified: 9233 / 10000
