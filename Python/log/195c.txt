3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 30, 3: 10} 

Starting training with batchsize 10 and learningrate 3.0
Epoch: 0; Identified: 1075 / 10000
Epoch: 1; Identified: 9121 / 10000
Epoch: 2; Identified: 9308 / 10000
Epoch: 3; Identified: 9341 / 10000
Epoch: 4; Identified: 9414 / 10000
Epoch: 5; Identified: 9404 / 10000
Epoch: 6; Identified: 9479 / 10000
Epoch: 7; Identified: 9502 / 10000
Epoch: 8; Identified: 9441 / 10000
Epoch: 9; Identified: 9512 / 10000
Epoch: 10; Identified: 9520 / 10000
Epoch: 11; Identified: 9486 / 10000
Epoch: 12; Identified: 9557 / 10000
Epoch: 13; Identified: 9559 / 10000
Epoch: 14; Identified: 9544 / 10000
Epoch: 15; Identified: 9582 / 10000
Epoch: 16; Identified: 9578 / 10000
Epoch: 17; Identified: 9591 / 10000
Epoch: 18; Identified: 9570 / 10000
Epoch: 19; Identified: 9563 / 10000
Epoch: 20; Identified: 9551 / 10000
Epoch: 21; Identified: 9570 / 10000
Epoch: 22; Identified: 9587 / 10000
Epoch: 23; Identified: 9588 / 10000
Epoch: 24; Identified: 9603 / 10000
Epoch: 25; Identified: 9611 / 10000
Epoch: 26; Identified: 9607 / 10000
Epoch: 27; Identified: 9564 / 10000
Epoch: 28; Identified: 9590 / 10000
Epoch: 29; Identified: 9565 / 10000
Epoch: 30; Identified: 9593 / 10000
