3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 10, 3: 10} 

Starting training with batchsize 50 and learningrate 1.0
Epoch: 0; Identified: 1031 / 10000
Epoch: 1; Identified: 4706 / 10000
Epoch: 2; Identified: 6335 / 10000
Epoch: 3; Identified: 7530 / 10000
Epoch: 4; Identified: 8062 / 10000
Epoch: 5; Identified: 8361 / 10000
Epoch: 6; Identified: 8551 / 10000
Epoch: 7; Identified: 8657 / 10000
Epoch: 8; Identified: 8756 / 10000
Epoch: 9; Identified: 8817 / 10000
Epoch: 10; Identified: 8874 / 10000
Epoch: 11; Identified: 8907 / 10000
Epoch: 12; Identified: 8965 / 10000
Epoch: 13; Identified: 8971 / 10000
Epoch: 14; Identified: 8986 / 10000
Epoch: 15; Identified: 9017 / 10000
Epoch: 16; Identified: 9035 / 10000
Epoch: 17; Identified: 9047 / 10000
Epoch: 18; Identified: 9067 / 10000
Epoch: 19; Identified: 9092 / 10000
Epoch: 20; Identified: 9109 / 10000
Epoch: 21; Identified: 9112 / 10000
Epoch: 22; Identified: 9148 / 10000
Epoch: 23; Identified: 9159 / 10000
Epoch: 24; Identified: 9169 / 10000
Epoch: 25; Identified: 9171 / 10000
Epoch: 26; Identified: 9171 / 10000
Epoch: 27; Identified: 9186 / 10000
Epoch: 28; Identified: 9205 / 10000
Epoch: 29; Identified: 9200 / 10000
Epoch: 30; Identified: 9213 / 10000
