3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 10, 2: 10} 

Starting training with batchsize 10 and learningrate 0.5
Epoch: 0; Identified: 1332 / 10000
Epoch: 1; Identified: 7769 / 10000
Epoch: 2; Identified: 8404 / 10000
Epoch: 3; Identified: 8599 / 10000
Epoch: 4; Identified: 8717 / 10000
Epoch: 5; Identified: 8782 / 10000
Epoch: 6; Identified: 8830 / 10000
Epoch: 7; Identified: 8879 / 10000
Epoch: 8; Identified: 8910 / 10000
Epoch: 9; Identified: 8945 / 10000
Epoch: 10; Identified: 8961 / 10000
Epoch: 11; Identified: 8975 / 10000
Epoch: 12; Identified: 8984 / 10000
Epoch: 13; Identified: 9005 / 10000
Epoch: 14; Identified: 9030 / 10000
Epoch: 15; Identified: 9033 / 10000
Epoch: 16; Identified: 9039 / 10000
Epoch: 17; Identified: 9072 / 10000
Epoch: 18; Identified: 9071 / 10000
Epoch: 19; Identified: 9077 / 10000
Epoch: 20; Identified: 9085 / 10000
Epoch: 21; Identified: 9077 / 10000
Epoch: 22; Identified: 9094 / 10000
Epoch: 23; Identified: 9091 / 10000
Epoch: 24; Identified: 9078 / 10000
Epoch: 25; Identified: 9081 / 10000
Epoch: 26; Identified: 9098 / 10000
Epoch: 27; Identified: 9104 / 10000
Epoch: 28; Identified: 9091 / 10000
Epoch: 29; Identified: 9112 / 10000
Epoch: 30; Identified: 9089 / 10000
