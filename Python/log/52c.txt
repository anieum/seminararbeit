3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 60, 2: 10} 

Starting training with batchsize 10 and learningrate 5.0
Epoch: 0; Identified: 801 / 10000
Epoch: 1; Identified: 7556 / 10000
Epoch: 2; Identified: 8599 / 10000
Epoch: 3; Identified: 8546 / 10000
Epoch: 4; Identified: 9390 / 10000
Epoch: 5; Identified: 9434 / 10000
Epoch: 6; Identified: 9481 / 10000
Epoch: 7; Identified: 9490 / 10000
Epoch: 8; Identified: 9524 / 10000
Epoch: 9; Identified: 9481 / 10000
Epoch: 10; Identified: 9556 / 10000
Epoch: 11; Identified: 9530 / 10000
Epoch: 12; Identified: 9578 / 10000
Epoch: 13; Identified: 9569 / 10000
Epoch: 14; Identified: 9538 / 10000
Epoch: 15; Identified: 9596 / 10000
Epoch: 16; Identified: 9590 / 10000
Epoch: 17; Identified: 9599 / 10000
Epoch: 18; Identified: 9604 / 10000
Epoch: 19; Identified: 9609 / 10000
Epoch: 20; Identified: 9611 / 10000
Epoch: 21; Identified: 9608 / 10000
Epoch: 22; Identified: 9629 / 10000
Epoch: 23; Identified: 9611 / 10000
Epoch: 24; Identified: 9651 / 10000
Epoch: 25; Identified: 9609 / 10000
Epoch: 26; Identified: 9642 / 10000
Epoch: 27; Identified: 9637 / 10000
Epoch: 28; Identified: 9644 / 10000
Epoch: 29; Identified: 9628 / 10000
Epoch: 30; Identified: 9639 / 10000
