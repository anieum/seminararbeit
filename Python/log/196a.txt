3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 30, 3: 10} 

Starting training with batchsize 10 and learningrate 5.0
Epoch: 0; Identified: 1086 / 10000
Epoch: 1; Identified: 9169 / 10000
Epoch: 2; Identified: 9328 / 10000
Epoch: 3; Identified: 9398 / 10000
Epoch: 4; Identified: 9346 / 10000
Epoch: 5; Identified: 9379 / 10000
Epoch: 6; Identified: 9510 / 10000
Epoch: 7; Identified: 9514 / 10000
Epoch: 8; Identified: 9490 / 10000
Epoch: 9; Identified: 9509 / 10000
Epoch: 10; Identified: 9548 / 10000
Epoch: 11; Identified: 9529 / 10000
Epoch: 12; Identified: 9535 / 10000
Epoch: 13; Identified: 9516 / 10000
Epoch: 14; Identified: 9534 / 10000
Epoch: 15; Identified: 9525 / 10000
Epoch: 16; Identified: 9568 / 10000
Epoch: 17; Identified: 9567 / 10000
Epoch: 18; Identified: 9585 / 10000
Epoch: 19; Identified: 9572 / 10000
Epoch: 20; Identified: 9515 / 10000
Epoch: 21; Identified: 9572 / 10000
Epoch: 22; Identified: 9572 / 10000
Epoch: 23; Identified: 9582 / 10000
Epoch: 24; Identified: 9581 / 10000
Epoch: 25; Identified: 9570 / 10000
Epoch: 26; Identified: 9592 / 10000
Epoch: 27; Identified: 9571 / 10000
Epoch: 28; Identified: 9601 / 10000
Epoch: 29; Identified: 9569 / 10000
Epoch: 30; Identified: 9591 / 10000
