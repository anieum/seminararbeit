3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 30, 3: 10} 

Starting training with batchsize 10 and learningrate 1.0
Epoch: 0; Identified: 1032 / 10000
Epoch: 1; Identified: 8554 / 10000
Epoch: 2; Identified: 8902 / 10000
Epoch: 3; Identified: 9007 / 10000
Epoch: 4; Identified: 9011 / 10000
Epoch: 5; Identified: 9069 / 10000
Epoch: 6; Identified: 9115 / 10000
Epoch: 7; Identified: 9086 / 10000
Epoch: 8; Identified: 9137 / 10000
Epoch: 9; Identified: 9060 / 10000
Epoch: 10; Identified: 9156 / 10000
Epoch: 11; Identified: 9170 / 10000
Epoch: 12; Identified: 9179 / 10000
Epoch: 13; Identified: 9206 / 10000
Epoch: 14; Identified: 9194 / 10000
Epoch: 15; Identified: 9206 / 10000
Epoch: 16; Identified: 9215 / 10000
Epoch: 17; Identified: 9201 / 10000
Epoch: 18; Identified: 9212 / 10000
Epoch: 19; Identified: 9227 / 10000
Epoch: 20; Identified: 9222 / 10000
Epoch: 21; Identified: 9216 / 10000
Epoch: 22; Identified: 9220 / 10000
Epoch: 23; Identified: 9199 / 10000
Epoch: 24; Identified: 9203 / 10000
Epoch: 25; Identified: 9239 / 10000
Epoch: 26; Identified: 9215 / 10000
Epoch: 27; Identified: 9217 / 10000
Epoch: 28; Identified: 9243 / 10000
Epoch: 29; Identified: 9240 / 10000
Epoch: 30; Identified: 9218 / 10000
