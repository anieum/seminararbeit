3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 10, 3: 10} 

Starting training with batchsize 10 and learningrate 1.0
Epoch: 0; Identified: 999 / 10000
Epoch: 1; Identified: 8645 / 10000
Epoch: 2; Identified: 9019 / 10000
Epoch: 3; Identified: 9137 / 10000
Epoch: 4; Identified: 9208 / 10000
Epoch: 5; Identified: 9290 / 10000
Epoch: 6; Identified: 9318 / 10000
Epoch: 7; Identified: 9360 / 10000
Epoch: 8; Identified: 9361 / 10000
Epoch: 9; Identified: 9388 / 10000
Epoch: 10; Identified: 9368 / 10000
Epoch: 11; Identified: 9414 / 10000
Epoch: 12; Identified: 9424 / 10000
Epoch: 13; Identified: 9444 / 10000
Epoch: 14; Identified: 9426 / 10000
Epoch: 15; Identified: 9433 / 10000
Epoch: 16; Identified: 9465 / 10000
Epoch: 17; Identified: 9452 / 10000
Epoch: 18; Identified: 9464 / 10000
Epoch: 19; Identified: 9479 / 10000
Epoch: 20; Identified: 9479 / 10000
Epoch: 21; Identified: 9453 / 10000
Epoch: 22; Identified: 9471 / 10000
Epoch: 23; Identified: 9427 / 10000
Epoch: 24; Identified: 9462 / 10000
Epoch: 25; Identified: 9487 / 10000
Epoch: 26; Identified: 9457 / 10000
Epoch: 27; Identified: 9484 / 10000
Epoch: 28; Identified: 9454 / 10000
Epoch: 29; Identified: 9479 / 10000
Epoch: 30; Identified: 9451 / 10000
