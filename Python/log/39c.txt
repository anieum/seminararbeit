3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 30, 2: 10} 

Starting training with batchsize 10 and learningrate 3.0
Epoch: 0; Identified: 1008 / 10000
Epoch: 1; Identified: 9005 / 10000
Epoch: 2; Identified: 9173 / 10000
Epoch: 3; Identified: 9272 / 10000
Epoch: 4; Identified: 9332 / 10000
Epoch: 5; Identified: 9369 / 10000
Epoch: 6; Identified: 9364 / 10000
Epoch: 7; Identified: 9398 / 10000
Epoch: 8; Identified: 9384 / 10000
Epoch: 9; Identified: 9381 / 10000
Epoch: 10; Identified: 9398 / 10000
Epoch: 11; Identified: 9420 / 10000
Epoch: 12; Identified: 9398 / 10000
Epoch: 13; Identified: 9460 / 10000
Epoch: 14; Identified: 9447 / 10000
Epoch: 15; Identified: 9417 / 10000
Epoch: 16; Identified: 9451 / 10000
Epoch: 17; Identified: 9477 / 10000
Epoch: 18; Identified: 9457 / 10000
Epoch: 19; Identified: 9440 / 10000
Epoch: 20; Identified: 9476 / 10000
Epoch: 21; Identified: 9459 / 10000
Epoch: 22; Identified: 9488 / 10000
Epoch: 23; Identified: 9460 / 10000
Epoch: 24; Identified: 9498 / 10000
Epoch: 25; Identified: 9492 / 10000
Epoch: 26; Identified: 9453 / 10000
Epoch: 27; Identified: 9489 / 10000
Epoch: 28; Identified: 9486 / 10000
Epoch: 29; Identified: 9465 / 10000
Epoch: 30; Identified: 9470 / 10000
