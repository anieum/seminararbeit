3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 30, 2: 10} 

Starting training with batchsize 50 and learningrate 3.0
Epoch: 0; Identified: 1085 / 10000
Epoch: 1; Identified: 8396 / 10000
Epoch: 2; Identified: 8882 / 10000
Epoch: 3; Identified: 9013 / 10000
Epoch: 4; Identified: 9102 / 10000
Epoch: 5; Identified: 9149 / 10000
Epoch: 6; Identified: 9183 / 10000
Epoch: 7; Identified: 9211 / 10000
Epoch: 8; Identified: 9229 / 10000
Epoch: 9; Identified: 9255 / 10000
Epoch: 10; Identified: 9273 / 10000
Epoch: 11; Identified: 9289 / 10000
Epoch: 12; Identified: 9321 / 10000
Epoch: 13; Identified: 9323 / 10000
Epoch: 14; Identified: 9331 / 10000
Epoch: 15; Identified: 9350 / 10000
Epoch: 16; Identified: 9349 / 10000
Epoch: 17; Identified: 9347 / 10000
Epoch: 18; Identified: 9361 / 10000
Epoch: 19; Identified: 9370 / 10000
Epoch: 20; Identified: 9373 / 10000
Epoch: 21; Identified: 9393 / 10000
Epoch: 22; Identified: 9377 / 10000
Epoch: 23; Identified: 9389 / 10000
Epoch: 24; Identified: 9403 / 10000
Epoch: 25; Identified: 9399 / 10000
Epoch: 26; Identified: 9403 / 10000
Epoch: 27; Identified: 9408 / 10000
Epoch: 28; Identified: 9410 / 10000
Epoch: 29; Identified: 9419 / 10000
Epoch: 30; Identified: 9418 / 10000
