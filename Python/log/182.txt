3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 20, 3: 10} 

Starting training with batchsize 10 and learningrate 1.0
Epoch: 0; Identified: 899 / 10000
Epoch: 1; Identified: 8860 / 10000
Epoch: 2; Identified: 9154 / 10000
Epoch: 3; Identified: 9266 / 10000
Epoch: 4; Identified: 9290 / 10000
Epoch: 5; Identified: 9359 / 10000
Epoch: 6; Identified: 9362 / 10000
Epoch: 7; Identified: 9414 / 10000
Epoch: 8; Identified: 9415 / 10000
Epoch: 9; Identified: 9450 / 10000
Epoch: 10; Identified: 9439 / 10000
Epoch: 11; Identified: 9440 / 10000
Epoch: 12; Identified: 9467 / 10000
Epoch: 13; Identified: 9463 / 10000
Epoch: 14; Identified: 9468 / 10000
Epoch: 15; Identified: 9498 / 10000
Epoch: 16; Identified: 9503 / 10000
Epoch: 17; Identified: 9494 / 10000
Epoch: 18; Identified: 9493 / 10000
Epoch: 19; Identified: 9488 / 10000
Epoch: 20; Identified: 9499 / 10000
Epoch: 21; Identified: 9488 / 10000
Epoch: 22; Identified: 9506 / 10000
Epoch: 23; Identified: 9512 / 10000
Epoch: 24; Identified: 9507 / 10000
Epoch: 25; Identified: 9514 / 10000
Epoch: 26; Identified: 9495 / 10000
Epoch: 27; Identified: 9492 / 10000
Epoch: 28; Identified: 9507 / 10000
Epoch: 29; Identified: 9491 / 10000
Epoch: 30; Identified: 9486 / 10000
