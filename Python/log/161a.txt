3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 60, 3: 10} 

Starting training with batchsize 50 and learningrate 0.5
Epoch: 0; Identified: 829 / 10000
Epoch: 1; Identified: 4699 / 10000
Epoch: 2; Identified: 6598 / 10000
Epoch: 3; Identified: 7491 / 10000
Epoch: 4; Identified: 8076 / 10000
Epoch: 5; Identified: 8364 / 10000
Epoch: 6; Identified: 8520 / 10000
Epoch: 7; Identified: 8630 / 10000
Epoch: 8; Identified: 8699 / 10000
Epoch: 9; Identified: 8778 / 10000
Epoch: 10; Identified: 8823 / 10000
Epoch: 11; Identified: 8893 / 10000
Epoch: 12; Identified: 8939 / 10000
Epoch: 13; Identified: 8971 / 10000
Epoch: 14; Identified: 8990 / 10000
Epoch: 15; Identified: 9028 / 10000
Epoch: 16; Identified: 9055 / 10000
Epoch: 17; Identified: 9067 / 10000
Epoch: 18; Identified: 9079 / 10000
Epoch: 19; Identified: 9099 / 10000
Epoch: 20; Identified: 9110 / 10000
Epoch: 21; Identified: 9119 / 10000
Epoch: 22; Identified: 9137 / 10000
Epoch: 23; Identified: 9145 / 10000
Epoch: 24; Identified: 9160 / 10000
Epoch: 25; Identified: 9183 / 10000
Epoch: 26; Identified: 9175 / 10000
Epoch: 27; Identified: 9189 / 10000
Epoch: 28; Identified: 9195 / 10000
Epoch: 29; Identified: 9205 / 10000
Epoch: 30; Identified: 9223 / 10000
