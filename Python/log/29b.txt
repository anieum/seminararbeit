3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 20, 2: 10} 

Starting training with batchsize 50 and learningrate 0.5
Epoch: 0; Identified: 1162 / 10000
Epoch: 1; Identified: 4293 / 10000
Epoch: 2; Identified: 6244 / 10000
Epoch: 3; Identified: 7403 / 10000
Epoch: 4; Identified: 7973 / 10000
Epoch: 5; Identified: 8226 / 10000
Epoch: 6; Identified: 8413 / 10000
Epoch: 7; Identified: 8541 / 10000
Epoch: 8; Identified: 8620 / 10000
Epoch: 9; Identified: 8713 / 10000
Epoch: 10; Identified: 8749 / 10000
Epoch: 11; Identified: 8799 / 10000
Epoch: 12; Identified: 8838 / 10000
Epoch: 13; Identified: 8860 / 10000
Epoch: 14; Identified: 8896 / 10000
Epoch: 15; Identified: 8924 / 10000
Epoch: 16; Identified: 8937 / 10000
Epoch: 17; Identified: 8957 / 10000
Epoch: 18; Identified: 8976 / 10000
Epoch: 19; Identified: 8998 / 10000
Epoch: 20; Identified: 9012 / 10000
Epoch: 21; Identified: 9021 / 10000
Epoch: 22; Identified: 9027 / 10000
Epoch: 23; Identified: 9042 / 10000
Epoch: 24; Identified: 9046 / 10000
Epoch: 25; Identified: 9071 / 10000
Epoch: 26; Identified: 9071 / 10000
Epoch: 27; Identified: 9072 / 10000
Epoch: 28; Identified: 9081 / 10000
Epoch: 29; Identified: 9084 / 10000
Epoch: 30; Identified: 9092 / 10000
