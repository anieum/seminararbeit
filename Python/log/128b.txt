3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 20, 2: 60, 3: 10} 

Starting training with batchsize 50 and learningrate 5.0
Epoch: 0; Identified: 1145 / 10000
Epoch: 1; Identified: 7796 / 10000
Epoch: 2; Identified: 8111 / 10000
Epoch: 3; Identified: 9103 / 10000
Epoch: 4; Identified: 9206 / 10000
Epoch: 5; Identified: 9248 / 10000
Epoch: 6; Identified: 9249 / 10000
Epoch: 7; Identified: 9286 / 10000
Epoch: 8; Identified: 9309 / 10000
Epoch: 9; Identified: 9331 / 10000
Epoch: 10; Identified: 9351 / 10000
Epoch: 11; Identified: 9364 / 10000
Epoch: 12; Identified: 9377 / 10000
Epoch: 13; Identified: 9383 / 10000
Epoch: 14; Identified: 9397 / 10000
Epoch: 15; Identified: 9400 / 10000
Epoch: 16; Identified: 9410 / 10000
Epoch: 17; Identified: 9409 / 10000
Epoch: 18; Identified: 9391 / 10000
Epoch: 19; Identified: 9395 / 10000
Epoch: 20; Identified: 9408 / 10000
Epoch: 21; Identified: 9398 / 10000
Epoch: 22; Identified: 9417 / 10000
Epoch: 23; Identified: 9426 / 10000
Epoch: 24; Identified: 9424 / 10000
Epoch: 25; Identified: 9446 / 10000
Epoch: 26; Identified: 9426 / 10000
Epoch: 27; Identified: 9417 / 10000
Epoch: 28; Identified: 9432 / 10000
Epoch: 29; Identified: 9451 / 10000
Epoch: 30; Identified: 9428 / 10000
