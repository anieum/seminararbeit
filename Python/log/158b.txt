3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 60, 3: 10} 

Starting training with batchsize 10 and learningrate 1.0
Epoch: 0; Identified: 947 / 10000
Epoch: 1; Identified: 8893 / 10000
Epoch: 2; Identified: 9129 / 10000
Epoch: 3; Identified: 9236 / 10000
Epoch: 4; Identified: 9311 / 10000
Epoch: 5; Identified: 9343 / 10000
Epoch: 6; Identified: 9376 / 10000
Epoch: 7; Identified: 9372 / 10000
Epoch: 8; Identified: 9383 / 10000
Epoch: 9; Identified: 9404 / 10000
Epoch: 10; Identified: 9419 / 10000
Epoch: 11; Identified: 9448 / 10000
Epoch: 12; Identified: 9452 / 10000
Epoch: 13; Identified: 9483 / 10000
Epoch: 14; Identified: 9476 / 10000
Epoch: 15; Identified: 9449 / 10000
Epoch: 16; Identified: 9472 / 10000
Epoch: 17; Identified: 9502 / 10000
Epoch: 18; Identified: 9501 / 10000
Epoch: 19; Identified: 9504 / 10000
Epoch: 20; Identified: 9508 / 10000
Epoch: 21; Identified: 9502 / 10000
Epoch: 22; Identified: 9515 / 10000
Epoch: 23; Identified: 9534 / 10000
Epoch: 24; Identified: 9536 / 10000
Epoch: 25; Identified: 9505 / 10000
Epoch: 26; Identified: 9544 / 10000
Epoch: 27; Identified: 9532 / 10000
Epoch: 28; Identified: 9525 / 10000
Epoch: 29; Identified: 9528 / 10000
Epoch: 30; Identified: 9521 / 10000
