3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 20, 2: 60, 3: 10} 

Starting training with batchsize 10 and learningrate 1.0
Epoch: 0; Identified: 757 / 10000
Epoch: 1; Identified: 7845 / 10000
Epoch: 2; Identified: 8124 / 10000
Epoch: 3; Identified: 9014 / 10000
Epoch: 4; Identified: 9226 / 10000
Epoch: 5; Identified: 9244 / 10000
Epoch: 6; Identified: 9312 / 10000
Epoch: 7; Identified: 9346 / 10000
Epoch: 8; Identified: 9352 / 10000
Epoch: 9; Identified: 9346 / 10000
Epoch: 10; Identified: 9355 / 10000
Epoch: 11; Identified: 9365 / 10000
Epoch: 12; Identified: 9408 / 10000
Epoch: 13; Identified: 9408 / 10000
Epoch: 14; Identified: 9415 / 10000
Epoch: 15; Identified: 9419 / 10000
Epoch: 16; Identified: 9415 / 10000
Epoch: 17; Identified: 9433 / 10000
Epoch: 18; Identified: 9422 / 10000
Epoch: 19; Identified: 9429 / 10000
Epoch: 20; Identified: 9442 / 10000
Epoch: 21; Identified: 9432 / 10000
Epoch: 22; Identified: 9442 / 10000
Epoch: 23; Identified: 9431 / 10000
Epoch: 24; Identified: 9455 / 10000
Epoch: 25; Identified: 9437 / 10000
Epoch: 26; Identified: 9441 / 10000
Epoch: 27; Identified: 9447 / 10000
Epoch: 28; Identified: 9448 / 10000
Epoch: 29; Identified: 9467 / 10000
Epoch: 30; Identified: 9452 / 10000
