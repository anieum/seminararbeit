3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 60, 2: 10} 

Starting training with batchsize 10 and learningrate 3.0
Epoch: 0; Identified: 497 / 10000
Epoch: 1; Identified: 7483 / 10000
Epoch: 2; Identified: 7659 / 10000
Epoch: 3; Identified: 7755 / 10000
Epoch: 4; Identified: 8641 / 10000
Epoch: 5; Identified: 9393 / 10000
Epoch: 6; Identified: 9461 / 10000
Epoch: 7; Identified: 9484 / 10000
Epoch: 8; Identified: 9514 / 10000
Epoch: 9; Identified: 9499 / 10000
Epoch: 10; Identified: 9532 / 10000
Epoch: 11; Identified: 9552 / 10000
Epoch: 12; Identified: 9550 / 10000
Epoch: 13; Identified: 9532 / 10000
Epoch: 14; Identified: 9545 / 10000
Epoch: 15; Identified: 9555 / 10000
Epoch: 16; Identified: 9567 / 10000
Epoch: 17; Identified: 9555 / 10000
Epoch: 18; Identified: 9574 / 10000
Epoch: 19; Identified: 9581 / 10000
Epoch: 20; Identified: 9546 / 10000
Epoch: 21; Identified: 9579 / 10000
Epoch: 22; Identified: 9582 / 10000
Epoch: 23; Identified: 9585 / 10000
Epoch: 24; Identified: 9576 / 10000
Epoch: 25; Identified: 9593 / 10000
Epoch: 26; Identified: 9575 / 10000
Epoch: 27; Identified: 9590 / 10000
Epoch: 28; Identified: 9573 / 10000
Epoch: 29; Identified: 9596 / 10000
Epoch: 30; Identified: 9591 / 10000
