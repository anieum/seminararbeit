3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 30, 3: 10} 

Starting training with batchsize 100 and learningrate 1.0
Epoch: 0; Identified: 1094 / 10000
Epoch: 1; Identified: 3769 / 10000
Epoch: 2; Identified: 5396 / 10000
Epoch: 3; Identified: 6148 / 10000
Epoch: 4; Identified: 6410 / 10000
Epoch: 5; Identified: 6571 / 10000
Epoch: 6; Identified: 6716 / 10000
Epoch: 7; Identified: 7392 / 10000
Epoch: 8; Identified: 7667 / 10000
Epoch: 9; Identified: 7757 / 10000
Epoch: 10; Identified: 8844 / 10000
Epoch: 11; Identified: 8895 / 10000
Epoch: 12; Identified: 8945 / 10000
Epoch: 13; Identified: 8980 / 10000
Epoch: 14; Identified: 8994 / 10000
Epoch: 15; Identified: 9025 / 10000
Epoch: 16; Identified: 9062 / 10000
Epoch: 17; Identified: 9069 / 10000
Epoch: 18; Identified: 9107 / 10000
Epoch: 19; Identified: 9118 / 10000
Epoch: 20; Identified: 9125 / 10000
Epoch: 21; Identified: 9130 / 10000
Epoch: 22; Identified: 9153 / 10000
Epoch: 23; Identified: 9170 / 10000
Epoch: 24; Identified: 9160 / 10000
Epoch: 25; Identified: 9193 / 10000
Epoch: 26; Identified: 9188 / 10000
Epoch: 27; Identified: 9203 / 10000
Epoch: 28; Identified: 9202 / 10000
Epoch: 29; Identified: 9227 / 10000
Epoch: 30; Identified: 9216 / 10000
