3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 20, 2: 10, 3: 10} 

Starting training with batchsize 100 and learningrate 3.0
Epoch: 0; Identified: 1316 / 10000
Epoch: 1; Identified: 6311 / 10000
Epoch: 2; Identified: 8006 / 10000
Epoch: 3; Identified: 8498 / 10000
Epoch: 4; Identified: 8733 / 10000
Epoch: 5; Identified: 8828 / 10000
Epoch: 6; Identified: 8899 / 10000
Epoch: 7; Identified: 8940 / 10000
Epoch: 8; Identified: 8987 / 10000
Epoch: 9; Identified: 9011 / 10000
Epoch: 10; Identified: 9037 / 10000
Epoch: 11; Identified: 9075 / 10000
Epoch: 12; Identified: 9122 / 10000
Epoch: 13; Identified: 9121 / 10000
Epoch: 14; Identified: 9121 / 10000
Epoch: 15; Identified: 9167 / 10000
Epoch: 16; Identified: 9166 / 10000
Epoch: 17; Identified: 9196 / 10000
Epoch: 18; Identified: 9196 / 10000
Epoch: 19; Identified: 9205 / 10000
Epoch: 20; Identified: 9210 / 10000
Epoch: 21; Identified: 9213 / 10000
Epoch: 22; Identified: 9222 / 10000
Epoch: 23; Identified: 9213 / 10000
Epoch: 24; Identified: 9243 / 10000
Epoch: 25; Identified: 9225 / 10000
Epoch: 26; Identified: 9253 / 10000
Epoch: 27; Identified: 9244 / 10000
Epoch: 28; Identified: 9257 / 10000
Epoch: 29; Identified: 9251 / 10000
Epoch: 30; Identified: 9247 / 10000
