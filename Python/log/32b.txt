3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 20, 2: 10} 

Starting training with batchsize 50 and learningrate 5.0
Epoch: 0; Identified: 571 / 10000
Epoch: 1; Identified: 8023 / 10000
Epoch: 2; Identified: 8964 / 10000
Epoch: 3; Identified: 9102 / 10000
Epoch: 4; Identified: 9172 / 10000
Epoch: 5; Identified: 9181 / 10000
Epoch: 6; Identified: 9227 / 10000
Epoch: 7; Identified: 9253 / 10000
Epoch: 8; Identified: 9256 / 10000
Epoch: 9; Identified: 9278 / 10000
Epoch: 10; Identified: 9326 / 10000
Epoch: 11; Identified: 9335 / 10000
Epoch: 12; Identified: 9324 / 10000
Epoch: 13; Identified: 9327 / 10000
Epoch: 14; Identified: 9317 / 10000
Epoch: 15; Identified: 9351 / 10000
Epoch: 16; Identified: 9335 / 10000
Epoch: 17; Identified: 9311 / 10000
Epoch: 18; Identified: 9346 / 10000
Epoch: 19; Identified: 9346 / 10000
Epoch: 20; Identified: 9352 / 10000
Epoch: 21; Identified: 9346 / 10000
Epoch: 22; Identified: 9354 / 10000
Epoch: 23; Identified: 9372 / 10000
Epoch: 24; Identified: 9365 / 10000
Epoch: 25; Identified: 9372 / 10000
Epoch: 26; Identified: 9386 / 10000
Epoch: 27; Identified: 9388 / 10000
Epoch: 28; Identified: 9393 / 10000
Epoch: 29; Identified: 9379 / 10000
Epoch: 30; Identified: 9403 / 10000
