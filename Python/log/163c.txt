3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 60, 3: 10} 

Starting training with batchsize 50 and learningrate 3.0
Epoch: 0; Identified: 1269 / 10000
Epoch: 1; Identified: 8580 / 10000
Epoch: 2; Identified: 8938 / 10000
Epoch: 3; Identified: 9088 / 10000
Epoch: 4; Identified: 9158 / 10000
Epoch: 5; Identified: 9202 / 10000
Epoch: 6; Identified: 9266 / 10000
Epoch: 7; Identified: 9257 / 10000
Epoch: 8; Identified: 9305 / 10000
Epoch: 9; Identified: 9299 / 10000
Epoch: 10; Identified: 9329 / 10000
Epoch: 11; Identified: 9362 / 10000
Epoch: 12; Identified: 9393 / 10000
Epoch: 13; Identified: 9374 / 10000
Epoch: 14; Identified: 9397 / 10000
Epoch: 15; Identified: 9385 / 10000
Epoch: 16; Identified: 9415 / 10000
Epoch: 17; Identified: 9384 / 10000
Epoch: 18; Identified: 9419 / 10000
Epoch: 19; Identified: 9416 / 10000
Epoch: 20; Identified: 9434 / 10000
Epoch: 21; Identified: 9429 / 10000
Epoch: 22; Identified: 9443 / 10000
Epoch: 23; Identified: 9440 / 10000
Epoch: 24; Identified: 9439 / 10000
Epoch: 25; Identified: 9442 / 10000
Epoch: 26; Identified: 9447 / 10000
Epoch: 27; Identified: 9458 / 10000
Epoch: 28; Identified: 9455 / 10000
Epoch: 29; Identified: 9455 / 10000
Epoch: 30; Identified: 9451 / 10000
