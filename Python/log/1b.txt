3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 2 layers
Dimensions:  {0: 784, 1: 10} 

Starting training with batchsize 10 and learningrate 0.5
Epoch: 0; Identified: 1032 / 10000
Epoch: 1; Identified: 4664 / 10000
Epoch: 2; Identified: 5061 / 10000
Epoch: 3; Identified: 6456 / 10000
Epoch: 4; Identified: 6810 / 10000
Epoch: 5; Identified: 7784 / 10000
Epoch: 6; Identified: 8055 / 10000
Epoch: 7; Identified: 8145 / 10000
Epoch: 8; Identified: 8201 / 10000
Epoch: 9; Identified: 8225 / 10000
Epoch: 10; Identified: 8249 / 10000
Epoch: 11; Identified: 8278 / 10000
Epoch: 12; Identified: 8303 / 10000
Epoch: 13; Identified: 8284 / 10000
Epoch: 14; Identified: 8303 / 10000
Epoch: 15; Identified: 8309 / 10000
Epoch: 16; Identified: 8341 / 10000
Epoch: 17; Identified: 8315 / 10000
Epoch: 18; Identified: 8335 / 10000
Epoch: 19; Identified: 8334 / 10000
Epoch: 20; Identified: 9042 / 10000
Epoch: 21; Identified: 9111 / 10000
Epoch: 22; Identified: 9132 / 10000
Epoch: 23; Identified: 9123 / 10000
Epoch: 24; Identified: 9127 / 10000
Epoch: 25; Identified: 9150 / 10000
Epoch: 26; Identified: 9144 / 10000
Epoch: 27; Identified: 9144 / 10000
Epoch: 28; Identified: 9147 / 10000
Epoch: 29; Identified: 9157 / 10000
Epoch: 30; Identified: 9151 / 10000
