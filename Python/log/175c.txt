3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 10, 3: 10} 

Starting training with batchsize 50 and learningrate 3.0
Epoch: 0; Identified: 910 / 10000
Epoch: 1; Identified: 7996 / 10000
Epoch: 2; Identified: 8668 / 10000
Epoch: 3; Identified: 8921 / 10000
Epoch: 4; Identified: 9085 / 10000
Epoch: 5; Identified: 9186 / 10000
Epoch: 6; Identified: 9213 / 10000
Epoch: 7; Identified: 9207 / 10000
Epoch: 8; Identified: 9296 / 10000
Epoch: 9; Identified: 9310 / 10000
Epoch: 10; Identified: 9325 / 10000
Epoch: 11; Identified: 9325 / 10000
Epoch: 12; Identified: 9352 / 10000
Epoch: 13; Identified: 9386 / 10000
Epoch: 14; Identified: 9381 / 10000
Epoch: 15; Identified: 9391 / 10000
Epoch: 16; Identified: 9403 / 10000
Epoch: 17; Identified: 9398 / 10000
Epoch: 18; Identified: 9389 / 10000
Epoch: 19; Identified: 9372 / 10000
Epoch: 20; Identified: 9419 / 10000
Epoch: 21; Identified: 9415 / 10000
Epoch: 22; Identified: 9394 / 10000
Epoch: 23; Identified: 9402 / 10000
Epoch: 24; Identified: 9440 / 10000
Epoch: 25; Identified: 9438 / 10000
Epoch: 26; Identified: 9434 / 10000
Epoch: 27; Identified: 9421 / 10000
Epoch: 28; Identified: 9442 / 10000
Epoch: 29; Identified: 9442 / 10000
Epoch: 30; Identified: 9438 / 10000
