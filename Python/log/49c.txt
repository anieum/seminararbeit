3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 60, 2: 10} 

Starting training with batchsize 10 and learningrate 0.5
Epoch: 0; Identified: 861 / 10000
Epoch: 1; Identified: 5662 / 10000
Epoch: 2; Identified: 5802 / 10000
Epoch: 3; Identified: 5836 / 10000
Epoch: 4; Identified: 5880 / 10000
Epoch: 5; Identified: 6535 / 10000
Epoch: 6; Identified: 6601 / 10000
Epoch: 7; Identified: 6636 / 10000
Epoch: 8; Identified: 6695 / 10000
Epoch: 9; Identified: 7438 / 10000
Epoch: 10; Identified: 7468 / 10000
Epoch: 11; Identified: 7477 / 10000
Epoch: 12; Identified: 7496 / 10000
Epoch: 13; Identified: 7511 / 10000
Epoch: 14; Identified: 7536 / 10000
Epoch: 15; Identified: 7716 / 10000
Epoch: 16; Identified: 8395 / 10000
Epoch: 17; Identified: 8415 / 10000
Epoch: 18; Identified: 8430 / 10000
Epoch: 19; Identified: 8448 / 10000
Epoch: 20; Identified: 8446 / 10000
Epoch: 21; Identified: 8446 / 10000
Epoch: 22; Identified: 8478 / 10000
Epoch: 23; Identified: 8474 / 10000
Epoch: 24; Identified: 8489 / 10000
Epoch: 25; Identified: 8485 / 10000
Epoch: 26; Identified: 8494 / 10000
Epoch: 27; Identified: 8500 / 10000
Epoch: 28; Identified: 8510 / 10000
Epoch: 29; Identified: 8487 / 10000
Epoch: 30; Identified: 8512 / 10000
