3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 60, 3: 10} 

Starting training with batchsize 10 and learningrate 0.5
Epoch: 0; Identified: 812 / 10000
Epoch: 1; Identified: 7140 / 10000
Epoch: 2; Identified: 8693 / 10000
Epoch: 3; Identified: 8888 / 10000
Epoch: 4; Identified: 8961 / 10000
Epoch: 5; Identified: 8995 / 10000
Epoch: 6; Identified: 9032 / 10000
Epoch: 7; Identified: 9071 / 10000
Epoch: 8; Identified: 9094 / 10000
Epoch: 9; Identified: 9089 / 10000
Epoch: 10; Identified: 9121 / 10000
Epoch: 11; Identified: 9129 / 10000
Epoch: 12; Identified: 9155 / 10000
Epoch: 13; Identified: 9150 / 10000
Epoch: 14; Identified: 9169 / 10000
Epoch: 15; Identified: 9162 / 10000
Epoch: 16; Identified: 9190 / 10000
Epoch: 17; Identified: 9190 / 10000
Epoch: 18; Identified: 9185 / 10000
Epoch: 19; Identified: 9209 / 10000
Epoch: 20; Identified: 9200 / 10000
Epoch: 21; Identified: 9192 / 10000
Epoch: 22; Identified: 9206 / 10000
Epoch: 23; Identified: 9182 / 10000
Epoch: 24; Identified: 9217 / 10000
Epoch: 25; Identified: 9216 / 10000
Epoch: 26; Identified: 9223 / 10000
Epoch: 27; Identified: 9223 / 10000
Epoch: 28; Identified: 9224 / 10000
Epoch: 29; Identified: 9214 / 10000
Epoch: 30; Identified: 9230 / 10000
