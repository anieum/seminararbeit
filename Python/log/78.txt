3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 30, 3: 10} 

Starting training with batchsize 50 and learningrate 1.0
Epoch: 0; Identified: 664 / 10000
Epoch: 1; Identified: 5906 / 10000
Epoch: 2; Identified: 7407 / 10000
Epoch: 3; Identified: 7972 / 10000
Epoch: 4; Identified: 8239 / 10000
Epoch: 5; Identified: 8387 / 10000
Epoch: 6; Identified: 8497 / 10000
Epoch: 7; Identified: 8584 / 10000
Epoch: 8; Identified: 8628 / 10000
Epoch: 9; Identified: 8676 / 10000
Epoch: 10; Identified: 8710 / 10000
Epoch: 11; Identified: 8719 / 10000
Epoch: 12; Identified: 8765 / 10000
Epoch: 13; Identified: 8752 / 10000
Epoch: 14; Identified: 8790 / 10000
Epoch: 15; Identified: 8802 / 10000
Epoch: 16; Identified: 8827 / 10000
Epoch: 17; Identified: 8819 / 10000
Epoch: 18; Identified: 8853 / 10000
Epoch: 19; Identified: 8870 / 10000
Epoch: 20; Identified: 8858 / 10000
Epoch: 21; Identified: 8859 / 10000
Epoch: 22; Identified: 8909 / 10000
Epoch: 23; Identified: 8918 / 10000
Epoch: 24; Identified: 8910 / 10000
Epoch: 25; Identified: 8941 / 10000
Epoch: 26; Identified: 8926 / 10000
Epoch: 27; Identified: 8932 / 10000
Epoch: 28; Identified: 8937 / 10000
Epoch: 29; Identified: 8938 / 10000
Epoch: 30; Identified: 8955 / 10000
