3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 30, 3: 10} 

Starting training with batchsize 100 and learningrate 3.0
Epoch: 0; Identified: 1091 / 10000
Epoch: 1; Identified: 7220 / 10000
Epoch: 2; Identified: 7840 / 10000
Epoch: 3; Identified: 8034 / 10000
Epoch: 4; Identified: 8094 / 10000
Epoch: 5; Identified: 8186 / 10000
Epoch: 6; Identified: 8229 / 10000
Epoch: 7; Identified: 8245 / 10000
Epoch: 8; Identified: 8272 / 10000
Epoch: 9; Identified: 8314 / 10000
Epoch: 10; Identified: 8336 / 10000
Epoch: 11; Identified: 8360 / 10000
Epoch: 12; Identified: 8371 / 10000
Epoch: 13; Identified: 8387 / 10000
Epoch: 14; Identified: 8384 / 10000
Epoch: 15; Identified: 8419 / 10000
Epoch: 16; Identified: 8418 / 10000
Epoch: 17; Identified: 8426 / 10000
Epoch: 18; Identified: 8426 / 10000
Epoch: 19; Identified: 8456 / 10000
Epoch: 20; Identified: 8459 / 10000
Epoch: 21; Identified: 8466 / 10000
Epoch: 22; Identified: 8458 / 10000
Epoch: 23; Identified: 8478 / 10000
Epoch: 24; Identified: 8465 / 10000
Epoch: 25; Identified: 8481 / 10000
Epoch: 26; Identified: 8477 / 10000
Epoch: 27; Identified: 8488 / 10000
Epoch: 28; Identified: 8490 / 10000
Epoch: 29; Identified: 8493 / 10000
Epoch: 30; Identified: 8488 / 10000
