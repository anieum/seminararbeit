3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 60, 3: 10} 

Starting training with batchsize 10 and learningrate 1.0
Epoch: 0; Identified: 928 / 10000
Epoch: 1; Identified: 8817 / 10000
Epoch: 2; Identified: 9060 / 10000
Epoch: 3; Identified: 9169 / 10000
Epoch: 4; Identified: 9284 / 10000
Epoch: 5; Identified: 9299 / 10000
Epoch: 6; Identified: 9319 / 10000
Epoch: 7; Identified: 9329 / 10000
Epoch: 8; Identified: 9336 / 10000
Epoch: 9; Identified: 9368 / 10000
Epoch: 10; Identified: 9355 / 10000
Epoch: 11; Identified: 9374 / 10000
Epoch: 12; Identified: 9410 / 10000
Epoch: 13; Identified: 9393 / 10000
Epoch: 14; Identified: 9409 / 10000
Epoch: 15; Identified: 9427 / 10000
Epoch: 16; Identified: 9394 / 10000
Epoch: 17; Identified: 9417 / 10000
Epoch: 18; Identified: 9428 / 10000
Epoch: 19; Identified: 9420 / 10000
Epoch: 20; Identified: 9440 / 10000
Epoch: 21; Identified: 9430 / 10000
Epoch: 22; Identified: 9435 / 10000
Epoch: 23; Identified: 9442 / 10000
Epoch: 24; Identified: 9441 / 10000
Epoch: 25; Identified: 9438 / 10000
Epoch: 26; Identified: 9434 / 10000
Epoch: 27; Identified: 9460 / 10000
Epoch: 28; Identified: 9457 / 10000
Epoch: 29; Identified: 9457 / 10000
Epoch: 30; Identified: 9450 / 10000
