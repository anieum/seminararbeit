3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 10, 3: 10} 

Starting training with batchsize 10 and learningrate 0.5
Epoch: 0; Identified: 1086 / 10000
Epoch: 1; Identified: 7795 / 10000
Epoch: 2; Identified: 8504 / 10000
Epoch: 3; Identified: 8837 / 10000
Epoch: 4; Identified: 8997 / 10000
Epoch: 5; Identified: 9081 / 10000
Epoch: 6; Identified: 9131 / 10000
Epoch: 7; Identified: 9172 / 10000
Epoch: 8; Identified: 9220 / 10000
Epoch: 9; Identified: 9240 / 10000
Epoch: 10; Identified: 9249 / 10000
Epoch: 11; Identified: 9273 / 10000
Epoch: 12; Identified: 9305 / 10000
Epoch: 13; Identified: 9290 / 10000
Epoch: 14; Identified: 9318 / 10000
Epoch: 15; Identified: 9325 / 10000
Epoch: 16; Identified: 9334 / 10000
Epoch: 17; Identified: 9339 / 10000
Epoch: 18; Identified: 9331 / 10000
Epoch: 19; Identified: 9361 / 10000
Epoch: 20; Identified: 9363 / 10000
Epoch: 21; Identified: 9350 / 10000
Epoch: 22; Identified: 9358 / 10000
Epoch: 23; Identified: 9370 / 10000
Epoch: 24; Identified: 9352 / 10000
Epoch: 25; Identified: 9381 / 10000
Epoch: 26; Identified: 9380 / 10000
Epoch: 27; Identified: 9377 / 10000
Epoch: 28; Identified: 9370 / 10000
Epoch: 29; Identified: 9378 / 10000
Epoch: 30; Identified: 9383 / 10000
