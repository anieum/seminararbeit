3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 30, 3: 10} 

Starting training with batchsize 50 and learningrate 0.5
Epoch: 0; Identified: 951 / 10000
Epoch: 1; Identified: 5964 / 10000
Epoch: 2; Identified: 7258 / 10000
Epoch: 3; Identified: 7856 / 10000
Epoch: 4; Identified: 8219 / 10000
Epoch: 5; Identified: 8383 / 10000
Epoch: 6; Identified: 8566 / 10000
Epoch: 7; Identified: 8645 / 10000
Epoch: 8; Identified: 8742 / 10000
Epoch: 9; Identified: 8814 / 10000
Epoch: 10; Identified: 8848 / 10000
Epoch: 11; Identified: 8913 / 10000
Epoch: 12; Identified: 8941 / 10000
Epoch: 13; Identified: 8983 / 10000
Epoch: 14; Identified: 9006 / 10000
Epoch: 15; Identified: 9035 / 10000
Epoch: 16; Identified: 9066 / 10000
Epoch: 17; Identified: 9065 / 10000
Epoch: 18; Identified: 9085 / 10000
Epoch: 19; Identified: 9108 / 10000
Epoch: 20; Identified: 9115 / 10000
Epoch: 21; Identified: 9137 / 10000
Epoch: 22; Identified: 9152 / 10000
Epoch: 23; Identified: 9171 / 10000
Epoch: 24; Identified: 9174 / 10000
Epoch: 25; Identified: 9181 / 10000
Epoch: 26; Identified: 9191 / 10000
Epoch: 27; Identified: 9202 / 10000
Epoch: 28; Identified: 9200 / 10000
Epoch: 29; Identified: 9216 / 10000
Epoch: 30; Identified: 9224 / 10000
