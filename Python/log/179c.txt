3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 10, 3: 10} 

Starting training with batchsize 100 and learningrate 3.0
Epoch: 0; Identified: 1039 / 10000
Epoch: 1; Identified: 6600 / 10000
Epoch: 2; Identified: 7806 / 10000
Epoch: 3; Identified: 8366 / 10000
Epoch: 4; Identified: 8651 / 10000
Epoch: 5; Identified: 8803 / 10000
Epoch: 6; Identified: 8958 / 10000
Epoch: 7; Identified: 9005 / 10000
Epoch: 8; Identified: 9089 / 10000
Epoch: 9; Identified: 9139 / 10000
Epoch: 10; Identified: 9185 / 10000
Epoch: 11; Identified: 9223 / 10000
Epoch: 12; Identified: 9242 / 10000
Epoch: 13; Identified: 9275 / 10000
Epoch: 14; Identified: 9281 / 10000
Epoch: 15; Identified: 9301 / 10000
Epoch: 16; Identified: 9333 / 10000
Epoch: 17; Identified: 9315 / 10000
Epoch: 18; Identified: 9342 / 10000
Epoch: 19; Identified: 9353 / 10000
Epoch: 20; Identified: 9362 / 10000
Epoch: 21; Identified: 9373 / 10000
Epoch: 22; Identified: 9369 / 10000
Epoch: 23; Identified: 9379 / 10000
Epoch: 24; Identified: 9382 / 10000
Epoch: 25; Identified: 9382 / 10000
Epoch: 26; Identified: 9395 / 10000
Epoch: 27; Identified: 9394 / 10000
Epoch: 28; Identified: 9400 / 10000
Epoch: 29; Identified: 9406 / 10000
Epoch: 30; Identified: 9412 / 10000
