3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 20, 3: 10} 

Starting training with batchsize 10 and learningrate 1.0
Epoch: 0; Identified: 1010 / 10000
Epoch: 1; Identified: 8711 / 10000
Epoch: 2; Identified: 8996 / 10000
Epoch: 3; Identified: 9075 / 10000
Epoch: 4; Identified: 9197 / 10000
Epoch: 5; Identified: 9270 / 10000
Epoch: 6; Identified: 9296 / 10000
Epoch: 7; Identified: 9341 / 10000
Epoch: 8; Identified: 9357 / 10000
Epoch: 9; Identified: 9396 / 10000
Epoch: 10; Identified: 9371 / 10000
Epoch: 11; Identified: 9376 / 10000
Epoch: 12; Identified: 9404 / 10000
Epoch: 13; Identified: 9416 / 10000
Epoch: 14; Identified: 9402 / 10000
Epoch: 15; Identified: 9401 / 10000
Epoch: 16; Identified: 9419 / 10000
Epoch: 17; Identified: 9437 / 10000
Epoch: 18; Identified: 9414 / 10000
Epoch: 19; Identified: 9409 / 10000
Epoch: 20; Identified: 9442 / 10000
Epoch: 21; Identified: 9408 / 10000
Epoch: 22; Identified: 9421 / 10000
Epoch: 23; Identified: 9455 / 10000
Epoch: 24; Identified: 9438 / 10000
Epoch: 25; Identified: 9445 / 10000
Epoch: 26; Identified: 9452 / 10000
Epoch: 27; Identified: 9427 / 10000
Epoch: 28; Identified: 9455 / 10000
Epoch: 29; Identified: 9467 / 10000
Epoch: 30; Identified: 9461 / 10000
