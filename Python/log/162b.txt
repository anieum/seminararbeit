3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 30, 2: 60, 3: 10} 

Starting training with batchsize 50 and learningrate 1.0
Epoch: 0; Identified: 1159 / 10000
Epoch: 1; Identified: 7619 / 10000
Epoch: 2; Identified: 8335 / 10000
Epoch: 3; Identified: 8630 / 10000
Epoch: 4; Identified: 8760 / 10000
Epoch: 5; Identified: 8847 / 10000
Epoch: 6; Identified: 8913 / 10000
Epoch: 7; Identified: 8961 / 10000
Epoch: 8; Identified: 9009 / 10000
Epoch: 9; Identified: 9045 / 10000
Epoch: 10; Identified: 9093 / 10000
Epoch: 11; Identified: 9103 / 10000
Epoch: 12; Identified: 9142 / 10000
Epoch: 13; Identified: 9151 / 10000
Epoch: 14; Identified: 9159 / 10000
Epoch: 15; Identified: 9172 / 10000
Epoch: 16; Identified: 9192 / 10000
Epoch: 17; Identified: 9195 / 10000
Epoch: 18; Identified: 9215 / 10000
Epoch: 19; Identified: 9235 / 10000
Epoch: 20; Identified: 9249 / 10000
Epoch: 21; Identified: 9240 / 10000
Epoch: 22; Identified: 9266 / 10000
Epoch: 23; Identified: 9251 / 10000
Epoch: 24; Identified: 9268 / 10000
Epoch: 25; Identified: 9279 / 10000
Epoch: 26; Identified: 9292 / 10000
Epoch: 27; Identified: 9305 / 10000
Epoch: 28; Identified: 9300 / 10000
Epoch: 29; Identified: 9319 / 10000
Epoch: 30; Identified: 9294 / 10000
