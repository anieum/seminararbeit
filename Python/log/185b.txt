3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 60, 2: 20, 3: 10} 

Starting training with batchsize 50 and learningrate 0.5
Epoch: 0; Identified: 1105 / 10000
Epoch: 1; Identified: 5902 / 10000
Epoch: 2; Identified: 7315 / 10000
Epoch: 3; Identified: 7926 / 10000
Epoch: 4; Identified: 8262 / 10000
Epoch: 5; Identified: 8466 / 10000
Epoch: 6; Identified: 8624 / 10000
Epoch: 7; Identified: 8722 / 10000
Epoch: 8; Identified: 8797 / 10000
Epoch: 9; Identified: 8875 / 10000
Epoch: 10; Identified: 8943 / 10000
Epoch: 11; Identified: 8987 / 10000
Epoch: 12; Identified: 9014 / 10000
Epoch: 13; Identified: 9040 / 10000
Epoch: 14; Identified: 9067 / 10000
Epoch: 15; Identified: 9079 / 10000
Epoch: 16; Identified: 9112 / 10000
Epoch: 17; Identified: 9124 / 10000
Epoch: 18; Identified: 9152 / 10000
Epoch: 19; Identified: 9165 / 10000
Epoch: 20; Identified: 9183 / 10000
Epoch: 21; Identified: 9186 / 10000
Epoch: 22; Identified: 9198 / 10000
Epoch: 23; Identified: 9204 / 10000
Epoch: 24; Identified: 9203 / 10000
Epoch: 25; Identified: 9223 / 10000
Epoch: 26; Identified: 9226 / 10000
Epoch: 27; Identified: 9231 / 10000
Epoch: 28; Identified: 9235 / 10000
Epoch: 29; Identified: 9252 / 10000
Epoch: 30; Identified: 9254 / 10000
