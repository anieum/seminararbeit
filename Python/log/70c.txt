3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 20, 3: 10} 

Starting training with batchsize 100 and learningrate 1.0
Epoch: 0; Identified: 1009 / 10000
Epoch: 1; Identified: 3945 / 10000
Epoch: 2; Identified: 5474 / 10000
Epoch: 3; Identified: 6600 / 10000
Epoch: 4; Identified: 7251 / 10000
Epoch: 5; Identified: 7722 / 10000
Epoch: 6; Identified: 7975 / 10000
Epoch: 7; Identified: 8208 / 10000
Epoch: 8; Identified: 8331 / 10000
Epoch: 9; Identified: 8433 / 10000
Epoch: 10; Identified: 8533 / 10000
Epoch: 11; Identified: 8592 / 10000
Epoch: 12; Identified: 8643 / 10000
Epoch: 13; Identified: 8670 / 10000
Epoch: 14; Identified: 8712 / 10000
Epoch: 15; Identified: 8748 / 10000
Epoch: 16; Identified: 8778 / 10000
Epoch: 17; Identified: 8813 / 10000
Epoch: 18; Identified: 8810 / 10000
Epoch: 19; Identified: 8848 / 10000
Epoch: 20; Identified: 8873 / 10000
Epoch: 21; Identified: 8890 / 10000
Epoch: 22; Identified: 8918 / 10000
Epoch: 23; Identified: 8914 / 10000
Epoch: 24; Identified: 8939 / 10000
Epoch: 25; Identified: 8937 / 10000
Epoch: 26; Identified: 8948 / 10000
Epoch: 27; Identified: 8959 / 10000
Epoch: 28; Identified: 8986 / 10000
Epoch: 29; Identified: 8991 / 10000
Epoch: 30; Identified: 8980 / 10000
