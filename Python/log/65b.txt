3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 20, 3: 10} 

Starting training with batchsize 50 and learningrate 0.5
Epoch: 0; Identified: 1081 / 10000
Epoch: 1; Identified: 4471 / 10000
Epoch: 2; Identified: 6022 / 10000
Epoch: 3; Identified: 6811 / 10000
Epoch: 4; Identified: 7316 / 10000
Epoch: 5; Identified: 7691 / 10000
Epoch: 6; Identified: 7964 / 10000
Epoch: 7; Identified: 8151 / 10000
Epoch: 8; Identified: 8296 / 10000
Epoch: 9; Identified: 8432 / 10000
Epoch: 10; Identified: 8510 / 10000
Epoch: 11; Identified: 8565 / 10000
Epoch: 12; Identified: 8614 / 10000
Epoch: 13; Identified: 8649 / 10000
Epoch: 14; Identified: 8689 / 10000
Epoch: 15; Identified: 8708 / 10000
Epoch: 16; Identified: 8746 / 10000
Epoch: 17; Identified: 8765 / 10000
Epoch: 18; Identified: 8790 / 10000
Epoch: 19; Identified: 8797 / 10000
Epoch: 20; Identified: 8829 / 10000
Epoch: 21; Identified: 8830 / 10000
Epoch: 22; Identified: 8845 / 10000
Epoch: 23; Identified: 8851 / 10000
Epoch: 24; Identified: 8877 / 10000
Epoch: 25; Identified: 8894 / 10000
Epoch: 26; Identified: 8905 / 10000
Epoch: 27; Identified: 8918 / 10000
Epoch: 28; Identified: 8920 / 10000
Epoch: 29; Identified: 8937 / 10000
Epoch: 30; Identified: 8954 / 10000
