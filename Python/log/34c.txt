3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 20, 2: 10} 

Starting training with batchsize 100 and learningrate 1.0
Epoch: 0; Identified: 1286 / 10000
Epoch: 1; Identified: 4346 / 10000
Epoch: 2; Identified: 5569 / 10000
Epoch: 3; Identified: 6807 / 10000
Epoch: 4; Identified: 7667 / 10000
Epoch: 5; Identified: 8008 / 10000
Epoch: 6; Identified: 8263 / 10000
Epoch: 7; Identified: 8443 / 10000
Epoch: 8; Identified: 8590 / 10000
Epoch: 9; Identified: 8674 / 10000
Epoch: 10; Identified: 8757 / 10000
Epoch: 11; Identified: 8816 / 10000
Epoch: 12; Identified: 8866 / 10000
Epoch: 13; Identified: 8893 / 10000
Epoch: 14; Identified: 8922 / 10000
Epoch: 15; Identified: 8947 / 10000
Epoch: 16; Identified: 8972 / 10000
Epoch: 17; Identified: 8993 / 10000
Epoch: 18; Identified: 9010 / 10000
Epoch: 19; Identified: 9033 / 10000
Epoch: 20; Identified: 9039 / 10000
Epoch: 21; Identified: 9055 / 10000
Epoch: 22; Identified: 9063 / 10000
Epoch: 23; Identified: 9081 / 10000
Epoch: 24; Identified: 9085 / 10000
Epoch: 25; Identified: 9100 / 10000
Epoch: 26; Identified: 9104 / 10000
Epoch: 27; Identified: 9125 / 10000
Epoch: 28; Identified: 9140 / 10000
Epoch: 29; Identified: 9150 / 10000
Epoch: 30; Identified: 9169 / 10000
