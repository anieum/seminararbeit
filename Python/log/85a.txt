3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 60, 3: 10} 

Starting training with batchsize 10 and learningrate 0.5
Epoch: 0; Identified: 1226 / 10000
Epoch: 1; Identified: 8156 / 10000
Epoch: 2; Identified: 8754 / 10000
Epoch: 3; Identified: 8881 / 10000
Epoch: 4; Identified: 8900 / 10000
Epoch: 5; Identified: 8980 / 10000
Epoch: 6; Identified: 8999 / 10000
Epoch: 7; Identified: 9040 / 10000
Epoch: 8; Identified: 9059 / 10000
Epoch: 9; Identified: 9072 / 10000
Epoch: 10; Identified: 9078 / 10000
Epoch: 11; Identified: 9109 / 10000
Epoch: 12; Identified: 9106 / 10000
Epoch: 13; Identified: 9139 / 10000
Epoch: 14; Identified: 9133 / 10000
Epoch: 15; Identified: 9129 / 10000
Epoch: 16; Identified: 9160 / 10000
Epoch: 17; Identified: 9174 / 10000
Epoch: 18; Identified: 9166 / 10000
Epoch: 19; Identified: 9172 / 10000
Epoch: 20; Identified: 9204 / 10000
Epoch: 21; Identified: 9189 / 10000
Epoch: 22; Identified: 9214 / 10000
Epoch: 23; Identified: 9182 / 10000
Epoch: 24; Identified: 9195 / 10000
Epoch: 25; Identified: 9209 / 10000
Epoch: 26; Identified: 9182 / 10000
Epoch: 27; Identified: 9221 / 10000
Epoch: 28; Identified: 9210 / 10000
Epoch: 29; Identified: 9201 / 10000
Epoch: 30; Identified: 9197 / 10000
