3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 20, 2: 30, 3: 10} 

Starting training with batchsize 100 and learningrate 1.0
Epoch: 0; Identified: 1004 / 10000
Epoch: 1; Identified: 5296 / 10000
Epoch: 2; Identified: 7187 / 10000
Epoch: 3; Identified: 7807 / 10000
Epoch: 4; Identified: 8179 / 10000
Epoch: 5; Identified: 8408 / 10000
Epoch: 6; Identified: 8544 / 10000
Epoch: 7; Identified: 8652 / 10000
Epoch: 8; Identified: 8726 / 10000
Epoch: 9; Identified: 8761 / 10000
Epoch: 10; Identified: 8825 / 10000
Epoch: 11; Identified: 8861 / 10000
Epoch: 12; Identified: 8894 / 10000
Epoch: 13; Identified: 8930 / 10000
Epoch: 14; Identified: 8949 / 10000
Epoch: 15; Identified: 8981 / 10000
Epoch: 16; Identified: 9002 / 10000
Epoch: 17; Identified: 9015 / 10000
Epoch: 18; Identified: 9023 / 10000
Epoch: 19; Identified: 9055 / 10000
Epoch: 20; Identified: 9062 / 10000
Epoch: 21; Identified: 9069 / 10000
Epoch: 22; Identified: 9076 / 10000
Epoch: 23; Identified: 9096 / 10000
Epoch: 24; Identified: 9096 / 10000
Epoch: 25; Identified: 9116 / 10000
Epoch: 26; Identified: 9119 / 10000
Epoch: 27; Identified: 9128 / 10000
Epoch: 28; Identified: 9129 / 10000
Epoch: 29; Identified: 9152 / 10000
Epoch: 30; Identified: 9156 / 10000
