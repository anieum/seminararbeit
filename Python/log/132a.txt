3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 20, 2: 60, 3: 10} 

Starting training with batchsize 100 and learningrate 5.0
Epoch: 0; Identified: 975 / 10000
Epoch: 1; Identified: 6047 / 10000
Epoch: 2; Identified: 6364 / 10000
Epoch: 3; Identified: 7320 / 10000
Epoch: 4; Identified: 7415 / 10000
Epoch: 5; Identified: 7466 / 10000
Epoch: 6; Identified: 7489 / 10000
Epoch: 7; Identified: 7536 / 10000
Epoch: 8; Identified: 8318 / 10000
Epoch: 9; Identified: 8340 / 10000
Epoch: 10; Identified: 8365 / 10000
Epoch: 11; Identified: 8392 / 10000
Epoch: 12; Identified: 8392 / 10000
Epoch: 13; Identified: 8403 / 10000
Epoch: 14; Identified: 8424 / 10000
Epoch: 15; Identified: 8446 / 10000
Epoch: 16; Identified: 8451 / 10000
Epoch: 17; Identified: 8458 / 10000
Epoch: 18; Identified: 8464 / 10000
Epoch: 19; Identified: 8469 / 10000
Epoch: 20; Identified: 9264 / 10000
Epoch: 21; Identified: 9316 / 10000
Epoch: 22; Identified: 9285 / 10000
Epoch: 23; Identified: 9349 / 10000
Epoch: 24; Identified: 9338 / 10000
Epoch: 25; Identified: 9338 / 10000
Epoch: 26; Identified: 9338 / 10000
Epoch: 27; Identified: 9349 / 10000
Epoch: 28; Identified: 9355 / 10000
Epoch: 29; Identified: 9343 / 10000
Epoch: 30; Identified: 9352 / 10000
