3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 2 layers
Dimensions:  {0: 784, 1: 10} 

Starting training with batchsize 10 and learningrate 3.0
Epoch: 0; Identified: 1356 / 10000
Epoch: 1; Identified: 6550 / 10000
Epoch: 2; Identified: 6642 / 10000
Epoch: 3; Identified: 7523 / 10000
Epoch: 4; Identified: 7526 / 10000
Epoch: 5; Identified: 7547 / 10000
Epoch: 6; Identified: 7551 / 10000
Epoch: 7; Identified: 7552 / 10000
Epoch: 8; Identified: 7550 / 10000
Epoch: 9; Identified: 7546 / 10000
Epoch: 10; Identified: 7567 / 10000
Epoch: 11; Identified: 7569 / 10000
Epoch: 12; Identified: 7581 / 10000
Epoch: 13; Identified: 8365 / 10000
Epoch: 14; Identified: 8387 / 10000
Epoch: 15; Identified: 8354 / 10000
Epoch: 16; Identified: 8385 / 10000
Epoch: 17; Identified: 8384 / 10000
Epoch: 18; Identified: 8401 / 10000
Epoch: 19; Identified: 8389 / 10000
Epoch: 20; Identified: 8378 / 10000
Epoch: 21; Identified: 8388 / 10000
Epoch: 22; Identified: 8377 / 10000
Epoch: 23; Identified: 8374 / 10000
Epoch: 24; Identified: 8370 / 10000
Epoch: 25; Identified: 8403 / 10000
Epoch: 26; Identified: 8372 / 10000
Epoch: 27; Identified: 8383 / 10000
Epoch: 28; Identified: 8398 / 10000
Epoch: 29; Identified: 8378 / 10000
Epoch: 30; Identified: 8381 / 10000
