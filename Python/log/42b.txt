3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 30, 2: 10} 

Starting training with batchsize 50 and learningrate 1.0
Epoch: 0; Identified: 1309 / 10000
Epoch: 1; Identified: 6656 / 10000
Epoch: 2; Identified: 8012 / 10000
Epoch: 3; Identified: 8393 / 10000
Epoch: 4; Identified: 8592 / 10000
Epoch: 5; Identified: 8726 / 10000
Epoch: 6; Identified: 8816 / 10000
Epoch: 7; Identified: 8885 / 10000
Epoch: 8; Identified: 8929 / 10000
Epoch: 9; Identified: 8960 / 10000
Epoch: 10; Identified: 9004 / 10000
Epoch: 11; Identified: 9055 / 10000
Epoch: 12; Identified: 9066 / 10000
Epoch: 13; Identified: 9092 / 10000
Epoch: 14; Identified: 9129 / 10000
Epoch: 15; Identified: 9150 / 10000
Epoch: 16; Identified: 9165 / 10000
Epoch: 17; Identified: 9183 / 10000
Epoch: 18; Identified: 9206 / 10000
Epoch: 19; Identified: 9207 / 10000
Epoch: 20; Identified: 9233 / 10000
Epoch: 21; Identified: 9250 / 10000
Epoch: 22; Identified: 9247 / 10000
Epoch: 23; Identified: 9261 / 10000
Epoch: 24; Identified: 9255 / 10000
Epoch: 25; Identified: 9279 / 10000
Epoch: 26; Identified: 9276 / 10000
Epoch: 27; Identified: 9287 / 10000
Epoch: 28; Identified: 9273 / 10000
Epoch: 29; Identified: 9290 / 10000
Epoch: 30; Identified: 9298 / 10000
