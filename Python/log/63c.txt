3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 4 layers
Dimensions:  {0: 784, 1: 10, 2: 20, 3: 10} 

Starting training with batchsize 10 and learningrate 3.0
Epoch: 0; Identified: 1389 / 10000
Epoch: 1; Identified: 8725 / 10000
Epoch: 2; Identified: 8906 / 10000
Epoch: 3; Identified: 9019 / 10000
Epoch: 4; Identified: 8953 / 10000
Epoch: 5; Identified: 9095 / 10000
Epoch: 6; Identified: 9106 / 10000
Epoch: 7; Identified: 9016 / 10000
Epoch: 8; Identified: 9076 / 10000
Epoch: 9; Identified: 9067 / 10000
Epoch: 10; Identified: 9120 / 10000
Epoch: 11; Identified: 9149 / 10000
Epoch: 12; Identified: 9150 / 10000
Epoch: 13; Identified: 9191 / 10000
Epoch: 14; Identified: 9136 / 10000
Epoch: 15; Identified: 9182 / 10000
Epoch: 16; Identified: 9190 / 10000
Epoch: 17; Identified: 9161 / 10000
Epoch: 18; Identified: 9163 / 10000
Epoch: 19; Identified: 9187 / 10000
Epoch: 20; Identified: 9132 / 10000
Epoch: 21; Identified: 9217 / 10000
Epoch: 22; Identified: 9184 / 10000
Epoch: 23; Identified: 9166 / 10000
Epoch: 24; Identified: 9193 / 10000
Epoch: 25; Identified: 9227 / 10000
Epoch: 26; Identified: 9202 / 10000
Epoch: 27; Identified: 9166 / 10000
Epoch: 28; Identified: 9184 / 10000
Epoch: 29; Identified: 9230 / 10000
Epoch: 30; Identified: 9212 / 10000
