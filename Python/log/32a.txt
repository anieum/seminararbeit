3.7.4 (tags/v3.7.4:e09359112e, Jul  8 2019, 20:34:20) [MSC v.1916 64 bit (AMD64)] 

Loading MNIST database
Trainingsamples: 50000 Testsamples: 10000
Initialized network with 3 layers
Dimensions:  {0: 784, 1: 20, 2: 10} 

Starting training with batchsize 50 and learningrate 5.0
Epoch: 0; Identified: 1074 / 10000
Epoch: 1; Identified: 8688 / 10000
Epoch: 2; Identified: 8912 / 10000
Epoch: 3; Identified: 9099 / 10000
Epoch: 4; Identified: 9154 / 10000
Epoch: 5; Identified: 9216 / 10000
Epoch: 6; Identified: 9209 / 10000
Epoch: 7; Identified: 9252 / 10000
Epoch: 8; Identified: 9286 / 10000
Epoch: 9; Identified: 9272 / 10000
Epoch: 10; Identified: 9291 / 10000
Epoch: 11; Identified: 9283 / 10000
Epoch: 12; Identified: 9293 / 10000
Epoch: 13; Identified: 9301 / 10000
Epoch: 14; Identified: 9326 / 10000
Epoch: 15; Identified: 9331 / 10000
Epoch: 16; Identified: 9342 / 10000
Epoch: 17; Identified: 9338 / 10000
Epoch: 18; Identified: 9340 / 10000
Epoch: 19; Identified: 9349 / 10000
Epoch: 20; Identified: 9328 / 10000
Epoch: 21; Identified: 9354 / 10000
Epoch: 22; Identified: 9338 / 10000
Epoch: 23; Identified: 9348 / 10000
Epoch: 24; Identified: 9354 / 10000
Epoch: 25; Identified: 9334 / 10000
Epoch: 26; Identified: 9340 / 10000
Epoch: 27; Identified: 9372 / 10000
Epoch: 28; Identified: 9366 / 10000
Epoch: 29; Identified: 9380 / 10000
Epoch: 30; Identified: 9371 / 10000
